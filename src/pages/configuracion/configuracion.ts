import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, App } from 'ionic-angular';

import { LoginPage } from '../index.page';


import { ConfiguracionProvider, UsuarioProvider } from '../../providers/index.provider';

/**
 * Generated class for the ConfiguracionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-configuracion',
  templateUrl: 'configuracion.html',
})
export class ConfiguracionPage {

  nav: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public _cp: ConfiguracionProvider,
  public _up: UsuarioProvider, private viewCtrl: ViewController, private app: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfiguracionPage');
  }

  cerraSesion(){

    this._up.cerrarSesion();
    this.nav = this.app.getRootNavById('n4');
    this.nav.setRoot( LoginPage );
  }

}
