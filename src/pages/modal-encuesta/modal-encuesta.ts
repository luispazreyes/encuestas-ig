import { Component, ViewChild } from '@angular/core';
import { EncuestasProvider } from '../../providers/index.provider';
import { IonicPage, NavController, NavParams, AlertController, Select } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

/**
 * Generated class for the ModalEncuestaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-encuesta',
  templateUrl: 'modal-encuesta.html',
})
export class ModalEncuestaPage {

  @ViewChild(Select) select: Select;

  hallazgo: string;
  algo_mas: string;
  coord_x: number;
  coord_y: number;
  sexo: string;
  pregunta1: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public _ep: EncuestasProvider, private alertCtrl: AlertController,
    private geolocation: Geolocation) {
  }

  geolocalizar(){

    let opciones = {
      enableHighAccuracy: true
    };

    this.geolocation.getCurrentPosition(opciones)
      .then((resp) => {

       this.coord_x = resp.coords.latitude;
       this.coord_y = resp.coords.longitude;
       
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  guardarEncuesta(){
    this._ep.guardarEncuesta({'hallazgo': this.hallazgo, 'pregunta1': this.pregunta1, 'coord_x': this.coord_x, 'coord_y': this.coord_y, 'sexo': this.sexo })
    .then( () => {
      this.alertCtrl.create({
        title: 'Éxito',
        message: 'Ha guardado la encuesta exitosamente',
        buttons: [{
          text: 'OK',
          role: 'cancel',
          handler: () => {
            this.navCtrl.pop();
          }
        }]
      }).present();
    });
  }

  ionViewDidLoad() {

  }

}
