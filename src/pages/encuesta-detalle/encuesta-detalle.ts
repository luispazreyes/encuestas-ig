import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform } from 'ionic-angular';

import { EncuestasProvider, ConfiguracionProvider } from '../../providers/index.provider';

@IonicPage()
@Component({
  selector: 'page-encuesta-detalle',
  templateUrl: 'encuesta-detalle.html',
})
export class EncuestaDetallePage {

  estructura_encuesta: any;
  encuesta_detalle: any;
  idx: number;
  dependencias: number[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public _ep: EncuestasProvider, private alertCtrl: AlertController,
    public _cp: ConfiguracionProvider ) {
    

      this.idx = this.navParams.get('idx');
      this._ep.cargar_storage(String(this.idx), this._ep.encuestas)
        .then( ( res: any[] ) =>{
          this.encuesta_detalle = res;
  
          this.estructura_encuesta = this._ep.encuestas_descargadas.filter( (item: any) => {
            return ( item.id == this.encuesta_detalle[this.idx].encuesta_id);
          });
      
          this.estructura_encuesta[0]['campos'].forEach( ( valor: any, indice, array )  => {
            this.dependencias.push(parseInt(valor.id));
      
            if ( Array.isArray(this.encuesta_detalle[this.idx][parseInt(valor.id)])) {
              this.encuesta_detalle[this.idx][parseInt(valor.id)].forEach( (element: string) => {
                this.estructura_encuesta[0]['campos'][indice]['value'] += `${element} `;
              });
            }else{
              this.estructura_encuesta[0]['campos'][indice]['value'] = this.encuesta_detalle[this.idx][parseInt(valor.id)];
            }
            
          });
      });

    }

    ionViewWillEnter(){
      this.idx = this.navParams.get('idx');
      this._ep.cargar_storage(String(this.idx), this._ep.encuestas)
        .then( ( res: any[] ) =>{
          this.encuesta_detalle = res;
  
          this.estructura_encuesta = this._ep.encuestas_descargadas.filter( (item: any) => {
            return ( item.id == this.encuesta_detalle[this.idx].encuesta_id);
          });
      
          this.estructura_encuesta[0]['campos'].forEach( ( valor: any, indice, array )  => {
            this.dependencias.push(parseInt(valor.id));
      
            if ( Array.isArray(this.encuesta_detalle[this.idx][parseInt(valor.id)])) {
              this.encuesta_detalle[this.idx][parseInt(valor.id)].forEach( (element: string) => {
                this.estructura_encuesta[0]['campos'][indice]['value'] += `${element} `;
              });
            }else{
              this.estructura_encuesta[0]['campos'][indice]['value'] = this.encuesta_detalle[this.idx][parseInt(valor.id)];
            }
            
          });
      });
  }
    




  // modificarEncuesta(){
  //   let contador_vacios = 0;
  //   let encuesta = {};

    

  //   for (let i = 0; i < this.estructura_encuesta.length; i++) {

  //       if ( this.estructura_encuesta[i].tipo === 'texto' && !this.estructura_encuesta[i].depende) {
  //         console.log( this.encuesta_detalle[this.estructura_encuesta[i].campo_id]);
  //         if ( this.encuesta_detalle[this.estructura_encuesta[i].campo_id].length === 0 ) {
  //           contador_vacios++;
  //         }else{
  //           encuesta[ this.estructura_encuesta[i].campo_id ] = this.encuesta_detalle[this.estructura_encuesta[i].campo_id];
  //         }
  //       }

  //       if ( this.estructura_encuesta[i].tipo === 'texto' && this.estructura_encuesta[i].depende) {
  //         if ( this.encuesta_detalle[this.estructura_encuesta[this.estructura_encuesta[i].depende].campo_id] === this.estructura_encuesta[i].dependencia ) {
  //           if ( this.encuesta_detalle[this.estructura_encuesta[i].campo_id].length === 0 ) {
  //             contador_vacios++;
  //           }else{
  //             encuesta[ this.estructura_encuesta[i].campo_id ] = this.encuesta_detalle[this.estructura_encuesta[i].campo_id];
  //           }
  //         }
  //       }

  //       if ( this.estructura_encuesta[i].tipo === 'combobox' && !this.estructura_encuesta[i].depende) {
  //         if ( this.encuesta_detalle[this.estructura_encuesta[i].campo_id].length === 0 ) {
  //           contador_vacios++;
  //         }else{
  //           encuesta[ this.estructura_encuesta[i].campo_id ] = this.encuesta_detalle[this.estructura_encuesta[i].campo_id];
  //         }
  //       }

  //       if ( this.estructura_encuesta[i].tipo === 'combobox' && this.estructura_encuesta[i].depende) {
  //         if ( this.encuesta_detalle[this.estructura_encuesta[this.estructura_encuesta[i].depende].campo_id] === this.estructura_encuesta[i].dependencia ) {
  //           if( this.encuesta_detalle[this.estructura_encuesta[i].campo_id] ){
  //             if ( this.encuesta_detalle[this.estructura_encuesta[i].campo_id].length === 0 ) {
  //               contador_vacios++;
  //             }else{
  //               encuesta[ this.estructura_encuesta[i].campo_id ] = this.encuesta_detalle[this.estructura_encuesta[i].campo_id];
  //             }
  //           }else{
  //             this.encuesta_detalle[this.estructura_encuesta[i].campo_id] = '';
  //             contador_vacios++;
  //           }
  //         }
  //       }

  //       if ( this.estructura_encuesta[i].tipo === 'geo' && !this.estructura_encuesta[i].depende) {
  //         if ( this.encuesta_detalle.x.length === 0 || this.encuesta_detalle.y.length === 0 ) {
  //           contador_vacios++;
  //         }else{
  //           encuesta[ 'x' ] = this.encuesta_detalle.x;
  //           encuesta[ 'y' ] = this.encuesta_detalle.y;
  //         }
  //       }

  //       // if ( this.estructura_encuesta[i].tipo === 'geo' && this.estructura_encuesta[i].depende) {
  //       //   if ( this.encuestas[ this.encuestas[i].depende].value === this.encuestas[i].dependencia ) {
  //       //     if ( this.encuestas[i].x.length === 0 || this.encuestas[i].y.length === 0 ) {
  //       //       contador_vacios++;
  //       //     }else{
  //       //       encuesta2[ 'x' ] = this.encuestas[i].x;
  //       //       encuesta2[ 'y' ] = this.encuestas[i].y;
  //       //     }
  //       //   }
  //       // }

  //       //Tengo que adaptar esto despues y solucionar el problema de los campos dobles con las coordenadas

  //       // if ( this.encuestas[i].tipo === 'geo' && !this.encuestas[i].depende) {
  //       //   if ( this.encuestas[i].x.length === 0 || this.encuestas[i].y.length === 0 ) {
  //       //     contador_vacios++;
  //       //   }else{
  //       //     encuesta[ 'x' ] = this.encuestas[i].x;
  //       //     encuesta[ 'y' ] = this.encuestas[i].y;
  //       //   }
  //       // }
  //       //
  //       // if ( this.encuestas[i].tipo === 'geo' && this.encuestas[i].depende) {
  //       //   if ( this.encuestas[ this.encuestas[i].depende].value === this.encuestas[i].dependencia ) {
  //       //     if ( this.encuestas[i].x.length === 0 || this.encuestas[i].y.length === 0 ) {
  //       //       contador_vacios++;
  //       //     }else{
  //       //       encuesta[ 'x' ] = this.encuestas[i].x;
  //       //       encuesta[ 'y' ] = this.encuestas[i].y;
  //       //     }
  //       //   }
  //       // }

  //       // if ( this.estructura_encuesta[i].tipo === 'radio' && !this.estructura_encuesta[i].depende) {
  //       //   if ( this.encuesta_detalle[this.estructura_encuesta[i].campo].length === 0 ) {
  //       //     contador_vacios++;
  //       //   }else{
  //       //     encuesta[ this.estructura_encuesta[i].campo ] = this.encuesta_detalle[this.estructura_encuesta[i].campo];
  //       //   }
  //       // }
  //       //
  //       // if ( this.estructura_encuesta[i].tipo === 'radio' && this.estructura_encuesta[i].depende) {
  //       //   if ( this.encuesta_detalle[this.estructura_encuesta[this.estructura_encuesta[i].depende].campo]  === this.estructura_encuesta[i].dependencia ) {
  //       //     if ( this.encuesta_detalle[this.estructura_encuesta[i].campo].length === 0 ) {
  //       //       contador_vacios++;
  //       //     }else{
  //       //       encuesta[ this.estructura_encuesta[i].campo ] =  this.encuesta_detalle[this.estructura_encuesta[i].campo];
  //       //     }
  //       //   }
  //       // }
  //       //
  //       // if ( this.estructura_encuesta[i].tipo === 'imagen' && !this.estructura_encuesta[i].depende) {
  //       //   if ( this.encuesta_detalle[this.estructura_encuesta[i].campo].length === 0 ) {
  //       //     //contador_vacios++;
  //       //   }else{
  //       //     encuesta[ this.estructura_encuesta[i].campo ] =  this.encuesta_detalle[this.estructura_encuesta[i].campo];
  //       //   }
  //       // }
  //       //
  //       // if ( this.estructura_encuesta[i].tipo === 'imagen' && this.estructura_encuesta[i].depende) {
  //       //   if ( this.encuesta_detalle[this.estructura_encuesta[this.estructura_encuesta[i].depende].campo]  === this.estructura_encuesta[i].dependencia ) {
  //       //     if ( this.encuesta_detalle[this.estructura_encuesta[i].campo].length === 0 ) {
  //       //       //contador_vacios++;
  //       //     }else{
  //       //       encuesta[ this.estructura_encuesta[i].campo ] = this.encuesta_detalle[this.estructura_encuesta[i].campo];
  //       //     }
  //       //   }
  //       // }
  //   }

  //   if ( contador_vacios != 0 ) {

  //     this.alertCtrl.create({
  //       title: 'Error!!!!',
  //       subTitle: 'No debe dejar campos en blanco',
  //       buttons: ['OK']
  //     }).present();
  //   }else{

  //     console.log(encuesta);

  //     this._ep.modificarEncuesta( this.encuesta_detalle, this.idx)
  //       .then( () => {
  //         this.alertCtrl.create({
  //           title: 'Éxito',
  //           message: 'Encuesta modificada exitosamente',
  //           buttons: [{
  //             text: 'Ok',
  //             role: 'cancel',
  //             handler: () => {
  //               this.navCtrl.pop();
  //             }
  //           }]
  //         }).present();
  //       });
  //   }

  //   console.log('Contador de vacios', contador_vacios);
  // }




}
