import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { EncuestasProvider, ConfiguracionProvider } from '../../providers/index.provider';

/**
 * Generated class for the EncuestaEnviadaDetallePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-encuesta-enviada-detalle',
  templateUrl: 'encuesta-enviada-detalle.html',
})
export class EncuestaEnviadaDetallePage {

  encuesta_detalle: any;
  idx: number;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public _ep: EncuestasProvider, public _cp: ConfiguracionProvider ) {
    this.idx = navParams.get('idx');
    this.encuesta_detalle = _ep.encuestas_enviadas[this.idx];

    console.log(this.encuesta_detalle);

    for ( var key in this.encuesta_detalle ) {

      if ( key.includes("_")) {
        let new_key = key.replace(/_/g, ' ');
        this.encuesta_detalle[new_key] = this.encuesta_detalle[key];
        delete this.encuesta_detalle[key];
      }

    }

    console.log( this.encuesta_detalle );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EncuestaEnviadaDetallePage');
  }

}
