import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EncuestaDescargarPreviewPage } from './encuesta-descargar-preview';

@NgModule({
  declarations: [
    EncuestaDescargarPreviewPage,
  ],
  imports: [
    IonicPageModule.forChild(EncuestaDescargarPreviewPage),
  ],
})
export class EncuestaDescargarPreviewPageModule {}
