import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'arrayinclude'
})

export class ArrayIncludePipe implements PipeTransform{
    transform(value: any[], arr?: any): boolean{
        
        if (!Array.isArray(value)){
            return arr.includes(value);
        } else {
            //console.log(value.some(v => arr.indexOf(v) !== -1));
        
            return value.some(v => arr.indexOf(v) !== -1);
        }
        
    }
}