import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, ActionSheetController, LoadingController } from 'ionic-angular';
import { EncuestasProvider, UsuarioProvider } from '../../providers/index.provider';
import { ModalEncuestaPage, EncuestaDetallePage, EncuestaDinamicaPage, ModalMapPage, EncuestaElegirPage } from '../index.page';

/**
 * Generated class for the EncuestasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-encuestas',
  templateUrl: 'encuestas.html',
})
export class EncuestasPage {

  modificar: boolean = false;
  seleccionados: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public _ep: EncuestasProvider, private alertCtrl: AlertController,
    private modalCtrl: ModalController, private actionSheetCtrl: ActionSheetController,
    private loadingCtrl: LoadingController, private _up: UsuarioProvider) {
      console.log( _ep.encuestas );
  }

  mostrarEncuestaDetalle( idx: number){
    this.navCtrl.push( EncuestaDetallePage, { idx: idx} );
  }

  mostrarModalMapa(){
    this.navCtrl.push( ModalMapPage );
  }

  enviarEncuesta( idx: number ){
    let loading = this.loadingCtrl.create({
      content: 'Enviando'
    });

    loading.present();

    this._ep.subirEncuestas( idx, this._up.id_usuario )
      .then( resp => {

        loading.dismiss();

        if (resp) {
          this._ep.removeEncuesta( idx );
          this.alertCtrl.create({
            title: 'Éxito',
            message: 'La encuesta ha sido subida con éxito',
            buttons: [{
              text: 'OK',
              role: 'cancel',
              handler: () => {

              }
            }]
          }).present();

        }else{
          this.alertCtrl.create({
            title: 'Error',
            message: 'La encuesta no se ha podido subir, se ha guardado en el teléfono',
            buttons: [{
              text: 'OK',
              role: 'cancel',
              handler: () => {

              }
            }]
          }).present();
        }
      });
  }

  presentActionSheet() {
   let actionSheet = this.actionSheetCtrl.create({
     title: '¿Qué desea hacer con las encuestas?',
     buttons: [
       {
         text: 'Borrar Seleccionados',
         role: 'destructive',
         icon: 'trash',
         handler: () => {
           let index_encuesta = 0;

           if (this.seleccionados.length != 0) {
             for (let i = 0; i < this.seleccionados.length; i++) {
                if (this.seleccionados[i]) {
                  this._ep.removeEncuesta(index_encuesta);
                  index_encuesta--;
                }
                index_encuesta++;
             }
             this.modificar = false;
           }else{
             this.alertCtrl.create({
               title: 'Error',
               message: 'No ha seleccionado  ninguna encuesta',
               buttons: [{
                 text: 'OK',
                 role: 'cancel'
               }]
             }).present();
           }
         }
       },
       {
         text: 'Subir Encuestas Seleccionadas',
         icon: 'cloud-upload',
         handler: () => {
           let index_encuesta = 0;
           let encuestas_subidas = 0;
           let subio_encuesta;

           console.log( this._ep.encuestas );

           if (this.seleccionados.length != 0) {

             for (let i = 0; i < this.seleccionados.length; i++) {
                if (this.seleccionados[i]) {
                  this._ep.subirEncuestas( index_encuesta, this._up.id_usuario );
                  this._ep.removeEncuesta( index_encuesta );
                  index_encuesta--;
                }
                index_encuesta++;
             }

             this.alertCtrl.create({
               title: 'Éxito',
               message: ` se han subido exitosamente.`,
               buttons: [{
                 text: 'OK',
                 role: 'cancel'
               }]
             }).present();

             this.modificar = false;

           }else{
             this.alertCtrl.create({
               title: 'Error',
               message: 'No ha seleccionado  ninguna encuesta',
               buttons: [{
                 text: 'OK',
                 role: 'cancel'
               }]
             }).present();
           }

         }
       },
       {
         text: 'Cancelar',
         role: 'cancel',
         handler: () => {
         }
       }
     ]
   });

   actionSheet.present();
 }

  borrarEncuesta(idx: number){
    this._ep.removeEncuesta(idx)
      .then( () => {
        this.alertCtrl.create({
          title: 'Éxito',
          message: 'Ha borrado la encuesta exitosamente',
          buttons: [{
            text: 'OK',
            role: 'cancel'
          }]
        }).present();
      });
  }

  mostrarModalEncuesta(){
    this.navCtrl.push( EncuestaElegirPage );
    // const modal = this.modalCtrl.create(EncuestaDinamicaPage);
    // modal.present();
  }

  doRefresh(refresher) {
   console.log('Begin async operation', refresher);

   setTimeout(() => {
     console.log('Async operation has ended');
     console.log( this._ep.encuestas );
     refresher.complete();
   }, 2000);
 }

}
