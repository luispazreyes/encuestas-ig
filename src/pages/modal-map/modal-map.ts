import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import leaflet from 'leaflet';
import * as esri from "esri-leaflet";

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment
} from '@ionic-native/google-maps';

/**
 * Generated class for the ModalMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-map',
  templateUrl: 'modal-map.html',
})
export class ModalMapPage {

  map: any;
  // map: GoogleMap;
  modificado: boolean = false;
  lat: any;
  lng: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private googleMaps: GoogleMaps, private viewCtrl: ViewController ) {
    this.lat = navParams.get('lat');
    this.lng = navParams.get('lng');
  }

  guardarCoordenadas(){
    this.navCtrl.getPrevious().data.lat = this.lat;
    this.navCtrl.getPrevious().data.lng = this.lng;
    this.navCtrl.pop();
  }

  cerrarModal(){
    this.viewCtrl.dismiss();
  }

  loadMap() {
    this.map = leaflet.map("map").fitWorld();
    leaflet.tileLayer('', {
      attributions: 'www.tphangout.com',
      maxZoom: 120
    }).addTo(this.map);

    esri.basemapLayer('Imagery').addTo( this.map );

    let markerGroup = leaflet.featureGroup();
    let marker: any = leaflet.marker([/*e.latitude, e.longitude*/ this.lat, this.lng], {draggable: 'true'}).on('dragend', (event) => {
      var marker = event.target;
      var position = marker.getLatLng();
      marker.setLatLng(new leaflet.LatLng(position.lat, position.lng),{draggable:'true'});
      this.lat = position.lat;
      this.lng = position.lng;
      this.map.panTo(new leaflet.LatLng(position.lat, position.lng))
    })
    markerGroup.addLayer(marker);
    this.map.addLayer(markerGroup);


    // let latLngs = [ marker.getLatLng() ];
    // let markerBounds = leaflet.latLngBounds( latLngs );
    // this.map.fitBounds( markerBounds );

    // this.map.setZoom(5);

    this.map.setView( {'lat': this.lat, 'lng': this.lng}, 17);

   //  this.map.locate({
   //   setView: true,
   //   maxZoom: 120
   // }).on('locationfound', (e) => {
   //    let markerGroup = leaflet.featureGroup();
   //    let marker: any = leaflet.marker([/*e.latitude, e.longitude*/ this.lat, this.lng]).on('click', () => {
   //      alert('Marker clicked');
   //    })
   //    markerGroup.addLayer(marker);
   //    this.map.addLayer(markerGroup);
   //    }).on('locationerror', (err) => {
   //      alert(err.message);
   //  })
  }

  // loadMap(){
  //
  //   // This code is necessary for browser
  //   Environment.setEnv({
  //     'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyBQajitniq3-h1aDn9UkDXeiadgwvtQ30A',
  //     'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyBQajitniq3-h1aDn9UkDXeiadgwvtQ30A'
  //   });
  //
  //   let mapOptions: GoogleMapOptions = {
  //     camera: {
  //       target: {
  //         lat: 10.0604792,//this.lat, // default location
  //         lng: -87.2504002//this.lng // default location
  //       },
  //         zoom: 18,
  //         tilt: 30
  //       }
  //     };
  //
  //     this.map = GoogleMaps.create('map_canvas', mapOptions);
  //
  //     // Wait the MAP_READY before using any methods.
  //     this.map.one(GoogleMapsEvent.MAP_READY)
  //     .then(() => {
  //       // Now you can use all methods safely.
  //       this.getPosition();
  //     })
  //     .catch(error =>{
  //       console.log(error);
  //     });
  //
  // }

  // getPosition(): void{
  //   this.map.getMyLocation()
  //   .then(response => {
  //     this.map.moveCamera({
  //       target: response.latLng
  //     });
  //     this.map.addMarker({
  //       title: 'My Position',
  //       icon: 'blue',
  //       animation: 'DROP',
  //       position: response.latLng
  //     });
  //   })
  //   .catch(error =>{
  //     console.log(error);
  //   });
  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalMapPage');
    this.loadMap();
  }

}
