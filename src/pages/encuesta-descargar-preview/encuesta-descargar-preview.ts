import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EncuestasProvider, ConfiguracionProvider } from '../../providers/index.provider';

/**
 * Generated class for the EncuestaDescargarPreviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-encuesta-descargar-preview',
  templateUrl: 'encuesta-descargar-preview.html',
})
export class EncuestaDescargarPreviewPage {

  encuesta_preview: any;
  idx: number;
  tipo_encuesta: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public _ep: EncuestasProvider, public _cp: ConfiguracionProvider ) {
    this.idx = navParams.get('idx');
    this.tipo_encuesta = navParams.get('tipo_encuesta');

    switch( this.tipo_encuesta ){
      case 'descargar':
        this.encuesta_preview = _ep.encuestas_descargar[  this.idx ];
        break;
      case 'descargada':
        this.encuesta_preview = _ep.encuestas_descargadas[  this.idx ];
    }


  }

  ionViewDidLoad() {

  }

}
