import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IonicSelectableModule } from 'ionic-selectable';
import { EncuestaDinamicaPage } from './encuesta-dinamica';

@NgModule({
  declarations: [
    EncuestaDinamicaPage,
  ],
  imports: [
    IonicPageModule.forChild(EncuestaDinamicaPage),
    IonicSelectableModule
  ],
})
export class EncuestaDinamicaPageModule {}
