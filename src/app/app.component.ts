import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { LocalNotifications } from '@ionic-native/local-notifications';

import { TabsPage, LoginPage } from '../pages/index.page';

import { PushnotificationProvider, UsuarioProvider } from '../providers/index.provider';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    public _pushProvider: PushnotificationProvider, private network: Network,
    private localNotifications: LocalNotifications, public _up: UsuarioProvider) {
    platform.ready().then(() => {

          statusBar.styleDefault();
          splashScreen.hide();

          //this._pushProvider.init_notifications();
          _up.cargarStorage()
            .then( () => {
              if ( _up.token) {
                this.rootPage = TabsPage;
              }else{
                this.rootPage = LoginPage;
              }
            })
    });
  }
}
