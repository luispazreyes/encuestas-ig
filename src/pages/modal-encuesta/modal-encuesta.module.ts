import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalEncuestaPage } from './modal-encuesta';

@NgModule({
  declarations: [
    ModalEncuestaPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalEncuestaPage),
  ],
})
export class ModalEncuestaPageModule {}
