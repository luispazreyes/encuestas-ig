import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EncuestaElegirPage } from './encuesta-elegir';

@NgModule({
  declarations: [
    EncuestaElegirPage,
  ],
  imports: [
    IonicPageModule.forChild(EncuestaElegirPage),
  ],
})
export class EncuestaElegirPageModule {}
