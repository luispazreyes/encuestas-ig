import { Http, URLSearchParams } from '@angular/http';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { catchError, retry } from 'rxjs/operators';
import { throwError } from "rxjs";
import { Platform, AlertController } from "ionic-angular";


import {timeout} from 'rxjs/operators/timeout';

import { UsuarioProvider } from '../index.provider';

import { URL_SERVICIOS } from '../../config/url.servicios';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

//import { Encuesta } from '../../interfaces/encuesta.interface';

@Injectable()
export class EncuestasProvider {

  encuestas: any[] = [];
  encuestas_descargadas: any[] = [];
  encuestas_descargadas_idx: number[] = [];
  encuestas_descargar: any[] = [];
  encuestas_enviadas: any[] = [];

  constructor(public http: HttpClient, private storage: Storage, private platform: Platform,
    private alertCtrl: AlertController) {
    this.cargar_storage('encuestas', this.encuestas)
      .then( ( res: any[] ) =>{
        this.encuestas = res;
      });
    this.cargar_storage('encuestas_descargadas', this.encuestas_descargadas)
      .then( ( res: any[] ) => {
        this.encuestas_descargadas = res;

        this.encuestas_descargadas.forEach( ( valor: any, indice, array ) => {
          this.encuestas_descargadas_idx.push( valor.id );
        });

      });
  }

  cargarEncuestas( when: string, usuario_id: string ){

    let url = `${ URL_SERVICIOS }/encuestas/${ when }/${ usuario_id }`;

    console.log( usuario_id );

    //let url = `http://localhost:8080/indicadores_rest/index.php/encuestas/${ when }/${ usuario_id }`;

    let promise = new Promise( ( resolve, reject ) => {
      this.http.get( url, {responseType: 'text'} )
        .map( res => res )
        .subscribe( (data: any) => {
          data = JSON.parse(data);

          if (!data.error) {
            this.encuestas_enviadas = data.encuestas;
            console.log(this.encuestas_enviadas);
            resolve(true);
          }

        })
    });

    return promise;

  }

  descargarEncuestas(){
    let url = `${ URL_SERVICIOS }/encuestas/obtener_encuestas`;

    let promise = new Promise( ( resolve, reject ) => {
      this.http.get( url, {responseType: 'text'} )
        .map( res => res )
        .subscribe( (data: any) => {
          data = JSON.parse(data);

          if (!data.error) {
            this.encuestas_descargar = data;
            console.log(this.encuestas_descargar);
            resolve(true);
          }

        })
    });

    return promise;
  }

  async subirEncuestas( idx: number, usuario_id: string ){

    let promise = await new Promise( ( resolve, reject ) =>{

      let data = new URLSearchParams();

      let url = `${ URL_SERVICIOS }/encuestas/guardar_encuestas`

      for ( var key in this.encuestas[idx] ) {

        if( Array.isArray( this.encuestas[idx][key]) ){
          this.encuestas[idx][key].forEach( ( valor: any, indice, array ) => {
            data.append( key, String( valor ) );
          });

        }else{
          data.append( key, String( this.encuestas[idx][key] ) );
        }

      }

      this.encuestas[idx]['usuario_id'] = usuario_id;

      console.log('Data para enviar', this.encuestas[idx]);

      this.http.post( url, this.encuestas[idx])
              // .pipe(
              //   retry(3), // retry a failed request up to 3 times
              //   catchError(this.handleError) // then handle the error
              // )
              // .map(res => res.json())
               .subscribe( (resp: any) =>{

                // if ( resp.status != 200) {
                //   resolve( false );
                // }else{
                  //respuesta = resp.json();

                  if( resp ){

                    resolve( true );

                  }else{
                    resolve( false );
                  }
                //}
            },
            (error: any) => {
              console.log( "Error", error );
              resolve( false );
            }
          );
    });

    return promise;

  }

  modificarEncuesta( encuesta: any, idx: number){

    let promesa = new Promise( (resolve, reject) => {
      this.encuestas[idx] = encuesta;
      this.guardarStorage( 'encuestas', this.encuestas );
      resolve();
    });

    return promesa;

  }

  guardarEncuestasDescargadas( encuesta: any ){

    let promesa = new Promise( (resolve, reject) => {
      this.encuestas_descargadas.push( encuesta );

      this.encuestas_descargadas.forEach( ( valor: any, indice, array ) => {
        this.encuestas_descargadas_idx.push( valor.id );
      });

      this.guardarStorage( 'encuestas_descargadas', this.encuestas_descargadas );

      resolve();
    });

    return promesa;
  }

  guardarEncuesta( encuesta: any){

    let promesa = new Promise( (resolve, reject) => {
      this.encuestas.push(encuesta);
      this.guardarStorage( 'encuestas', this.encuestas );
      resolve( this.encuestas.lastIndexOf(encuesta));
    });

    return promesa;
  }

  private guardarStorage( key: string, object: any ){

    if( this.platform.is("cordova") ){
      // dispositivo
      this.storage.set( key, object );

    }else{
      // computadora
      localStorage.setItem(key, JSON.stringify( object ) );

    }

  }

  cargar_storage( $key: string, object: any[] ){

    let promesa = new Promise( ( resolve, reject )=>{

      if( this.platform.is("cordova") ){
        // dispositivo
        this.storage.ready()
                  .then( ()=>{

                  this.storage.get($key)
                          .then( encuestas =>{
                            if( encuestas ){
                              object = encuestas;
                            }
                            resolve( object );
                        })

              })


      }else{
        // computadora
        if( localStorage.getItem($key) ){
          //Existe encuestas en el localstorage

          console.log(typeof JSON.parse( localStorage.getItem($key)  ) )

          object = JSON.parse( localStorage.getItem($key)  );
        }

        resolve( object );

      }

    });

    return promesa;

  }

  removeEncuesta( idx:number ){

    let promesa = new Promise( (resolve, remove) => {
      this.encuestas.splice(idx,1);
      this.guardarStorage( 'encuestas', this.encuestas );
      resolve();
    });

    return promesa;

  }

  removeEncuestaDescargada( idx: number ){

    let promesa = new Promise( (resolve, remove) => {
      this.encuestas_descargadas.splice(idx,1);
      this.guardarStorage( 'encuestas_descargadas', this.encuestas_descargadas );

      this.encuestas_descargadas_idx = [];

      this.encuestas_descargadas.forEach( ( valor: any, indice, array ) => {
        this.encuestas_descargadas_idx.push( valor.id );
      });

      resolve();
    });

    return promesa;

  }

  // getEncuesta():any[]{

  //   Encuesta.forEach( (item: any) => {

  //     if (item.value) {
  //       item.value = '';
  //     }


  //     if (item.x || item.y) {
  //       item.x = '';
  //       item.y = '';
  //     }
  //     // console.log(item);
  //   });

  //   return Encuesta;
  // }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

}
