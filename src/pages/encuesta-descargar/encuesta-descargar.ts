import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, LoadingController } from 'ionic-angular';
import { EncuestasProvider } from '../../providers/index.provider';
import { EncuestaDescargarPreviewPage } from '../../pages/index.page';

/**
 * Generated class for the EncuestaDescargarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-encuesta-descargar',
  templateUrl: 'encuesta-descargar.html',
})
export class EncuestaDescargarPage {

  encuestas_para_descargar: boolean;
  encuestas_descargadas: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public _ep: EncuestasProvider, private actionSheetCtrl: ActionSheetController,
    private loadingCtrl: LoadingController) {
  }

  presentActionSheet() {
    this.actionSheetCtrl.create({
      title: 'Filtrar',
      buttons: [
        {
          text: 'Descargar Encuestas',
          //icon: 'cloud-upload',
          handler: () => {

            let loading = this.loadingCtrl.create({
              content: 'Cargando encuestas'
            });

            loading.present();

            this.encuestas_descargadas = false;
            this.encuestas_para_descargar = true;
            console.log()

            loading.dismiss(this.encuestas_para_descargar);

          }
        },{
          text: 'Encuestas Descargadas',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Cargando encuestas'
            });

            loading.present();

            this.encuestas_para_descargar = false;
            this.encuestas_descargadas = true;

            loading.dismiss();

          }
        },
      ]
    }).present();
  }


  previsualizarEncuesta( idx: number, tipo_encuesta: string ){
    this.navCtrl.push( EncuestaDescargarPreviewPage, { idx: idx, tipo_encuesta: tipo_encuesta} );
  }

  ionViewDidLoad() {

    let loading = this.loadingCtrl.create({
      content: 'Cargando encuestas'
    });

    loading.present();

    this._ep.descargarEncuestas()
      .then( () => {
        loading.dismiss();
      });

    this.encuestas_para_descargar = true;
    this.encuestas_descargadas = false;
  }

}
