import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer  } from '@angular/platform-browser';


@Pipe({
  name: 'domseguro'
})
export class DomseguroPipe implements PipeTransform {

  constructor( private domSanitizer: DomSanitizer ) { }

  transform( value: any ): any {

    const url = 'font-size: ';
    const px = 'px;';
    // console.log(this.domSanitizer.bypassSecurityTrustStyle( url + value + px));
    return this.domSanitizer.bypassSecurityTrustStyle(  value );
  }

}
