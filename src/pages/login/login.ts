import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';

import { TabsPage } from '../../pages/index.page';

import { UsuarioProvider } from '../../providers/usuario/usuario';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  usuario: string = '';
  contrasena: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private viewCtrl: ViewController, private _up: UsuarioProvider,
    private loadingCtrl: LoadingController) {
  }

  ingresar(){

    let loading = this.loadingCtrl.create({
      content: 'Verificando credenciales'
    });

    loading.present();

    this._up.ingresar( this.usuario, this.contrasena)
      .subscribe( () => {
        if (this._up.activo()) {
          this.navCtrl.setRoot( TabsPage );
        }
        loading.dismiss();
      });
  }

}
