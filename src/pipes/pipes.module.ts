import { NgModule } from '@angular/core';
import { DomseguroPipe } from './domseguro/domseguro';
import { ArrayIncludePipe } from './arrayinclude/arrayinclude.pipe';
@NgModule({
    declarations: [DomseguroPipe,
        ArrayIncludePipe],
	imports: [],
    exports: [DomseguroPipe,
        ArrayIncludePipe]
})
export class PipesModule {}
