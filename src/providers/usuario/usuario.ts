import { Http, URLSearchParams } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { throwError } from "rxjs";

import { URL_SERVICIOS } from '../../config/url.servicios';

import { AlertController, Platform } from 'ionic-angular';

// Plugin del sotrage
import { Storage } from '@ionic/storage';
/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuarioProvider {

  token: string;
  id_usuario: string;

  constructor(public http: HttpClient, private alertCtrl: AlertController,
    private platform: Platform, private storage: Storage) {

    this.cargarStorage();

  }

  activo(): boolean{
    if (this.token) {
      return true;
    }else{
      return false;
    }
  }

  ingresar( usuario: string, contrasena: string ){
    // let data = new HttpParams();
    // data = data.append('correo', correo);
    // data = data.append('contrasena', contrasena);
    //
     let url = URL_SERVICIOS + '/login';

     //let url = 'http://localhost:8080/indicadores_rest/index.php/login/';

    let data = {
      'usuario': usuario,
      'contrasenia': contrasena
    }

    return this.http.post(url,  data )
      .map( (res: any) => {

        let data_res: any = res;
        console.log(data_res);

        if (data_res.error) {
          this.alertCtrl.create({
            title: 'Error al iniciar',
            subTitle: data_res.mensaje,
            buttons:['OK']
          }).present();
        }else{
          this.token = data_res.token;
          this.id_usuario = data_res.id_usuario;

          //Guardar Storage
          this.guardarStorage();
        }
      });
  }

  cerrarSesion(){
    this.token = null;
    this.id_usuario = null;

    //guardar Storage
    this.guardarStorage();

  }

  private guardarStorage(){
    if (this.platform.is('cordova')) {
      //dispositivo
      this.storage.set( 'token', this.token );
      this.storage.set( 'id_usuario', this.id_usuario );
    }else{
      // computadora
      if (this.token) {
        localStorage.setItem('token', this.token);
        localStorage.setItem('id_usuario', this.id_usuario);
      }else{
        localStorage.removeItem('token');
        localStorage.removeItem('id_usuario');
      }
    }
  }

  cargarStorage(){

    let promesa = new Promise( (resolve, reject) => {
      if (this.platform.is('cordova')) {
        // Dispositivo
        this.storage.ready()
          .then( () => {
            this.storage.get('token')
              .then( (token: any) => {
                if (token) {
                  this.token = token;
                }
              });

              this.storage.get('id_usuario')
                .then( (id_usuario: any) => {
                  if (id_usuario) {
                    this.id_usuario = id_usuario;
                  }
                  resolve();
                });

          });

      }else{
        //Computadora
        if (localStorage.getItem('token')) {
        //Existe item en el local storage
            this.token = localStorage.getItem('token');
            this.id_usuario = localStorage.getItem('id_usuario');
            console.log('Id Usuario', this.id_usuario);

        }
        resolve();
      }
    });

    return promesa;

  }
}
