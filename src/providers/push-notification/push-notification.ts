import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal';
import { Http, URLSearchParams } from '@angular/http';
import { map } from 'rxjs/operators';
import { Platform } from 'ionic-angular';



@Injectable()
export class PushnotificationProvider {

  notificaciones: any[] = [];

  constructor( private oneSignal: OneSignal,
               public platform: Platform,
               public http: Http,) {
    console.log('Hello PushnotificationProvider Provider');
  }

  init_notifications(){
    if( this.platform.is('cordova') ){

      this.oneSignal.startInit('5dd271e1-494e-4d4c-983c-8714affeca24', '232557535066');

      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

      this.oneSignal.handleNotificationReceived().subscribe(() => {
       // do something when notification is received
       console.log('Notificacion recibida');
      });

      this.oneSignal.handleNotificationOpened().subscribe(() => {
        // do something when a notification is opened
        console.log('Notificacion abierta');
      });

      this.oneSignal.endInit();

    }else{
      console.log('OneSignal no funciona en Chrome');
    }
  }

  obtenerNotificaciones(){

    let promise = new Promise( (resolve, reject) => {
      let url ='https://zamoranorest.herokuapp.com/index.php/prueba/obtener_notificaciones';

      this.http.get( url )
                .pipe(map( resp => resp.json() ))
                .subscribe( data =>{

                  if( data.error ){
                    // manejar el error
                  }else{

                    //console.log(data);
                    data = JSON.parse(data);
                    this.notificaciones = data.notifications;
                    resolve();
                  }

          });
    });

    return promise;
  }

}
