import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, LoadingController } from 'ionic-angular';

import { EncuestasProvider, UsuarioProvider } from '../../providers/index.provider';

import { EncuestaEnviadaDetallePage } from '../../pages/index.page';

/**
 * Generated class for the EncuestasEnviadasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-encuestas-enviadas',
  templateUrl: 'encuestas-enviadas.html',
})
export class EncuestasEnviadasPage {

  encuestas_when: string = 'encuestas_todas';
  title: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private actionSheetCtrl: ActionSheetController, public _ep: EncuestasProvider,
    public _up: UsuarioProvider, private loadingCtrl: LoadingController ) {

  }

  mostrarEncuestaDetalle( idx: number){
    this.navCtrl.push( EncuestaEnviadaDetallePage, { idx: idx} );
  }

  presentActionSheet() {
    this.actionSheetCtrl.create({
      title: 'Filtrar',
      buttons: [
        {
          text: 'Todas las encuestas',
          //icon: 'cloud-upload',
          handler: () => {

            let loading = this.loadingCtrl.create({
              content: 'Cargando encuestas'
            });

            loading.present();

            this.encuestas_when = 'encuestas_todas';
            this.title = 'Todas las Encuestas Enviadas';
            this._ep.cargarEncuestas( this.encuestas_when, this._up.id_usuario )
              .then( () => {
                loading.dismiss();
              });
            console.log('Destructive clicked');
          }
        },{
          text: 'Hoy',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Cargando encuestas'
            });

            loading.present();

            this.encuestas_when = 'encuestas_hoy';
            this.title = 'Encuestas Enviadas Hoy';
            this._ep.cargarEncuestas( this.encuestas_when, this._up.id_usuario )
              .then( () => {
                loading.dismiss();
              });
            console.log('Archive clicked');
          }
        },
      ]
    }).present();
  }

  ionViewDidLoad() {

    let loading = this.loadingCtrl.create({
      content: 'Cargando encuestas'
    });

    loading.present();

    this.title = 'Todas las Encuestas Enviadas';
    this._ep.cargarEncuestas( this.encuestas_when, this._up.id_usuario )
      .then( () => {
        loading.dismiss();
      });
    console.log('ionViewDidLoad EncuestasEnviadasPage');
  }

  doRefresh(refresher) {
   console.log('Begin async operation', refresher);

   setTimeout(() => {
     this._ep.cargarEncuestas( this.encuestas_when, this._up.id_usuario )
     console.log('Async operation has ended');
     refresher.complete();
   }, 2000);
 }

}
