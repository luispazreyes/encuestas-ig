import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EncuestasProvider } from '../../providers/index.provider';
import { EncuestaDinamicaPage } from '../../pages/index.page';

/**
 * Generated class for the EncuestaElegirPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-encuesta-elegir',
  templateUrl: 'encuesta-elegir.html',
})
export class EncuestaElegirPage {

  current_date: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private _ep: EncuestasProvider) {
      this.current_date = new Date().toISOString().slice(0,10); 
  }

  realizarEncuesta( idx: number ){
    this.navCtrl.pop();
    this.navCtrl.push( EncuestaDinamicaPage , { idx_encuesta: idx} );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EncuestaElegirPage');
  }

}
