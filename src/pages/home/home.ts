import { Component } from '@angular/core';
import { NavController, ModalController, AlertController, Platform } from 'ionic-angular';

import { LocalNotifications } from '@ionic-native/local-notifications';

import { LoginPage, ModalEncuestaPage } from '../index.page';
import { EncuestasProvider, PushnotificationProvider } from '../../providers/index.provider';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  user: any = {}
  data: any = { title:'', description:'', date:'', time:'' };
  rootPage: any = ModalEncuestaPage;

  constructor(private navCtrl: NavController, private modalCtrl: ModalController,
    public _ep: EncuestasProvider, private alertCtrl: AlertController,
    public _pp: PushnotificationProvider, private localNotifications: LocalNotifications,
    private platform: Platform) {
      _pp.obtenerNotificaciones()
        .then( () => {

        });
  }

  setSound() {
    if (this.platform.is('android')) {
      return 'file://assets/sounds/Rooster.mp3'
    } else {
      return 'file://assets/sounds/Rooster.caf'
    }
  }

  doRefresh(refresher) {
   console.log('Begin async operation', refresher);

   this._pp.obtenerNotificaciones()
    .then( () => {
      console.log('Async operation has ended');
      refresher.complete();
    });
 }


}
