import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';

// Pages
import { LoginPage, HomePage, ModalEncuestaPage,
         TabsPage, EncuestasPage, EncuestaDetallePage,
         EncuestaDinamicaPage, EncuestasEnviadasPage,
         EncuestaEnviadaDetallePage, ConfiguracionPage,
         ModalMapPage, EncuestaDescargarPage, EncuestaDescargarPreviewPage, EncuestaElegirPage } from '../pages/index.page';

//Providers
import {UsuarioProvider,
        PushnotificationProvider, EncuestasProvider,
        ClientesProvider, ConfiguracionProvider} from '../providers/index.provider';

//Angular Fire 2
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { firebaseConfig } from '../config/firebase.config';

// Plugins
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { AgmCoreModule } from '@agm/core';
import { OneSignal } from '@ionic-native/onesignal';
import { HttpClientModule } from '@angular/common/http';
import { Network } from '@ionic-native/network';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { Diagnostic } from '@ionic-native/diagnostic';
import { GoogleMaps } from '@ionic-native/google-maps';
import { Base64 } from '@ionic-native/base64';

// ionic Select URLSearchParams
import { IonicSelectableModule } from 'ionic-selectable';

import { DomseguroPipe } from '../pipes/domseguro/domseguro';
import { ArrayIncludePipe } from '../pipes/arrayinclude/arrayinclude.pipe';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ModalEncuestaPage,
    TabsPage,
    EncuestasPage,
    EncuestaDetallePage,
    EncuestaDinamicaPage,
    EncuestasEnviadasPage,
    EncuestaEnviadaDetallePage,
    ConfiguracionPage,
    DomseguroPipe,
    ModalMapPage,
    EncuestaDescargarPage,
    EncuestaDescargarPreviewPage,
    EncuestaElegirPage,
    ArrayIncludePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicSelectableModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Atras'
    }),
    AngularFireModule.initializeApp(firebaseConfig),
    IonicStorageModule.forRoot(),
    AngularFirestoreModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCmqsHh0GJKM72kbe9CSE6DgfntdJiDXPQ'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ModalEncuestaPage,
    TabsPage,
    EncuestasPage,
    EncuestaDetallePage,
    EncuestaDinamicaPage,
    EncuestasEnviadasPage,
    EncuestaEnviadaDetallePage,
    ConfiguracionPage,
    ModalMapPage,
    EncuestaDescargarPage,
    EncuestaDescargarPreviewPage,
    EncuestaElegirPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UsuarioProvider,
    Geolocation,
    PushnotificationProvider,
    OneSignal,
    EncuestasProvider,
    Network,
    LocalNotifications,
    Camera,
    ImagePicker,
    ClientesProvider,
    ConfiguracionProvider,
    Diagnostic,
    GoogleMaps,
    Base64
  ]
})
export class AppModule {}
