import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';

import { URL_SERVICIOS } from '../../config/url.servicios';

import { Clientes } from '../../Data/clientes.data';

import 'rxjs/add/operator/map';

/*
  Generated class for the ClientesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ClientesProvider {

  clientes: any[] = [];
  cliente: any = {};

  constructor( private http: Http ) {

  }

  cargarClientes(){
    // let url = `${ URL_SERVICIOS }/clientes/obtener_clientes`;
    //
    // this.http.get( url )
    //   .map( res => res )
    //   .subscribe( (data: any) => {
    //     data = JSON.parse(data._body);
    //
    //     if ( !data.error ) {
    //       this.clientes = data.clientes;
    //     }
    //
    //   })

    this.clientes = Clientes;
  }

}
