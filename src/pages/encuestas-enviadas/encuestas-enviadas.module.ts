import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EncuestasEnviadasPage } from './encuestas-enviadas';

@NgModule({
  declarations: [
    EncuestasEnviadasPage,
  ],
  imports: [
    IonicPageModule.forChild(EncuestasEnviadasPage),
  ],
})
export class EncuestasEnviadasPageModule {}
