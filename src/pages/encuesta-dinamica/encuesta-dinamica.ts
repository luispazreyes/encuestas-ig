import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,
         AlertController, LoadingController, Platform,
         ModalController, normalizeURL} from 'ionic-angular';
import { DomSanitizer} from '@angular/platform-browser';

//Providers
import { EncuestasProvider, UsuarioProvider, ConfiguracionProvider } from '../../providers/index.provider';

//Páginas
import { ModalMapPage, ModalEncuestaPage } from '../index.page';

//Plugins
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Base64 } from '@ionic-native/base64';

//No se por que me dice que no lo encuentra pero funciona bien
import { IonicSelectableComponent  } from 'ionic-selectable';
import { Departamentos } from '../../Data/departamentos.data';
import { Municipios } from '../../Data/municipios.data';

@IonicPage()
@Component({
  selector: 'page-encuesta-dinamica',
  templateUrl: 'encuesta-dinamica.html',
})
export class EncuestaDinamicaPage{

  encuestas: any[];
  dependencias: number[] = [];
  disable_button: boolean = true;
  idx: string;

  departamentos: string[] = Departamentos;
  municipios: any = Municipios;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private alertCtrl: AlertController, private camera: Camera,
    private imagePicker: ImagePicker, private geolocation: Geolocation,
    public _ep: EncuestasProvider, private loadingCtrl: LoadingController,
    public _up: UsuarioProvider, public _cp: ConfiguracionProvider,
    private platform: Platform, private diagnostic: Diagnostic,
    private modalCtrl: ModalController, private sanitizer: DomSanitizer,
    private base64: Base64) {

      let idx_encuesta =navParams.get( 'idx_encuesta');
      this.encuestas = _ep.encuestas_descargadas[ idx_encuesta ];
      

      this.encuestas['campos'].forEach( ( valor: any, indice, array )  => {
        this.dependencias.push(parseInt(valor.id));
        valor.value = '';
      });   
      
      this.encuestas['x'] = null;
      this.encuestas['y'] = null;

      //console.log(this.encuestas);
      
  }

  clienteChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    //this.cliente = event.value.id;
    //console.log('port:', event.value);
  }

  mostrarModalMapa( idx: number ){

    this.navCtrl.push( ModalMapPage, { lat: this.encuestas['x'], lng: this.encuestas['y'] } );
    this.idx = 'geolocalizar';

  }

  geolocalizar( idx: number ){

    let loading = this.loadingCtrl.create({
      content: 'Obteniendo coordenadas'
    });

    loading.present();


    let opciones = {
      enableHighAccuracy: true,
      timeout: 20000,
      maximumAge: 0
    };

   this.platform.ready().then(() => {

     this.geolocation.getCurrentPosition(/*success, error,*/ opciones)
       .then((resp: any) => {
        this.encuestas['x'] = resp.coords.latitude;
        this.encuestas['y'] = resp.coords.longitude;
        loading.dismiss();

        this.disable_button = false;

        this.idx = 'geolocalizar';
        this.navCtrl.push( ModalMapPage,   { lat: resp.coords.latitude, lng: resp.coords.longitude} );

        //this.navCtrl.push( ModalMapPage,   { lat: resp.coords.latitude, lng: resp.coords.longitude} );

         // const modal = this.modalCtrl.create( ModalMapPage, { lat: resp.coords.latitude, lng: resp.coords.longitude} );
         // modal.present();

     }).catch((error) => {
       console.log('Error getting location', error);
       loading.dismiss();
     });
   });


  }

  borrarFoto( idx: number){
    this.encuestas['campos'][ idx ].value = '';
  }

  seleccionar_foto( idx: number ){

    let opciones:ImagePickerOptions = {
      //quality: 500,
      outputType: 0,
      maximumImagesCount: 1
    }


    this.imagePicker.getPictures(opciones).then((results) => {

      for (var i = 0; i < results.length; i++) {

          this.base64.encodeFile(results[i])
            .then( (base64File: string) => {
              this.encuestas['campos'][ idx ].value = this.sanitizer.bypassSecurityTrustUrl(base64File);
            }, (err)=>{
              console.log(err);
            });

      }

    }, (err) => {

      console.log( "ERROR en selector", JSON.stringify(err) );

    });


  }

  mostrar_camara( idx: number ){

    const options: CameraOptions = {
        quality: 50,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }

    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64:
     this.encuestas['campos'][ idx ].value = 'data:image/jpeg;base64,' + imageData;

    }, (err) => {
     // Handle error
     console.log( "ERROR EN CAMARA", JSON.stringify(err) );
    });

  }

  
  public ionViewWillEnter() {
    if (this.idx) {
      this.encuestas['x'] = this.navParams.get('lat')|| null;
      this.encuestas['y'] = this.navParams.get('lng')|| null;
    }

}

cleanComponent( comp: any){

  //this.encuestas['campos'][this.dependencias.indexOf(comp.id)]
  //console.log(comp.id);
  
}

  guardarEncuesta(){

    console.log(this.encuestas['campos']);
    

    let loading = this.loadingCtrl.create({
      content: 'Verificando'
    });

    loading.present();

    let contador_vacios = 0;
    let preguntas_sin_responder = '';
    let encuesta = {};
    let encuesta2 = {};
    let len = this.encuestas['campos'].length;
    
    for (let i = 0; i < len; i++) {


        if ( this.encuestas['campos'][i].tipo_pregunta === 'text' && !this.encuestas['campos'][i].depende) {
          if ( this.encuestas['campos'][i].value.length === 0 ||  this.encuestas['campos'][i].value === null) {
            contador_vacios++;
            preguntas_sin_responder += (i+1)+', ';
          }else{
            encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
            encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
          }
        }

        // if ( this.encuestas['campos'][i].tipo_pregunta === 'text' && this.encuestas['campos'][i].depende) {
        //   if ( this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value === this.encuestas['campos'][i].dependencia ) {
        //     if ( this.encuestas['campos'][i].value.length === 0 ) {
        //       contador_vacios++;
        //       preguntas_sin_responder += (i+1)+', ';
        //     }else{
        //       encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
        //       encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
        //     }
        //   } else {
        //     console.log('atencion', this.encuestas['campos'][i].dependencias);
            
        //   }
        // }

        if ( this.encuestas['campos'][i].tipo_pregunta === 'text' && this.encuestas['campos'][i].depende) {
          // console.log('valido!!!!!!', this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value);
          // console.error('Es valido?', this.encuestas['campos'][i].dependencias.includes(this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value[0]));
          
          let cont = 0;

          if ( Array.isArray(this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value)) {
            this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value.forEach( (value: string) => {
            
              if ( i === 14) {
                console.log('Valor', value);
                console.log('Dependencias', this.encuestas['campos'][i].dependencias);
                console.log('Es Valido?', this.encuestas['campos'][i].dependencias.includes(value));
                console.log('Field Value', this.encuestas['campos'][i].value);
                console.log('Length', this.encuestas['campos'][i].value.length);
                
                
              }

              if (this.encuestas['campos'][i].dependencias.includes(value)) {
                cont++; 
              }
            });
          

            if ( this.encuestas['campos'][i].dependencias.length === cont ) {
              if ( this.encuestas['campos'][i].value.length === 0 ) {
                contador_vacios++;
                preguntas_sin_responder += (i+1)+', ';
              }else{
                encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
                encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
              } 
            }
          }else if ( this.encuestas['campos'][i].dependencias.includes(this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value)) {
            if ( this.encuestas['campos'][i].value.length === 0 ) {
              contador_vacios++;
              preguntas_sin_responder += (i+1)+', ';
            }else{
              encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
              encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
            }
          }
        }

        //Enteros

        if ( this.encuestas['campos'][i].tipo_pregunta === 'integer' && !this.encuestas['campos'][i].depende) {
          if ( this.encuestas['campos'][i].value.length === 0 ) {
            contador_vacios++;
            preguntas_sin_responder += (i+1)+', ';
          }else{
            encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
            encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
          }
        }

        // if ( this.encuestas['campos'][i].tipo_pregunta === 'integer' && this.encuestas['campos'][i].depende) {
        //   if ( this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value === this.encuestas['campos'][i].dependencia ) {
        //     if ( this.encuestas['campos'][i].value.length === 0 ) {
        //       contador_vacios++;
        //       preguntas_sin_responder += (i+1)+', ';
        //     }else{
        //       encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
        //       encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
        //     }
        //   }
        // }

        if ( this.encuestas['campos'][i].tipo_pregunta === 'integer' && this.encuestas['campos'][i].depende) {
          if ( this.encuestas['campos'][i].dependencias.includes(this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value) ) {
            if ( this.encuestas['campos'][i].value.length === 0 ) {
              contador_vacios++;
              preguntas_sin_responder += (i+1)+', ';
            }else{
              encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
              encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
            }
          }
        }

        //Aquí termina los enteros

        if ( this.encuestas['campos'][i].tipo_pregunta === 'select_one [list_name]' && !this.encuestas['campos'][i].depende) {
          if ( this.encuestas['campos'][i].value.length === 0 ) {
            contador_vacios++;
            preguntas_sin_responder += (i+1)+', ';
          }else{
            encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
            encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
          }
        }

        // if ( this.encuestas['campos'][i].tipo_pregunta === 'select_one [list_name]' && this.encuestas['campos'][i].depende) {
        //   if ( this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value === this.encuestas['campos'][i].dependencia ) {
        //     if ( this.encuestas['campos'][i].value.length === 0 ) {
        //       contador_vacios++;
        //       preguntas_sin_responder += (i+1)+', ';
        //     }else{
        //       encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
        //       encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
        //     }
        //   }
        // }

        if ( this.encuestas['campos'][i].tipo_pregunta === 'select_one [list_name]' && this.encuestas['campos'][i].depende) {
          if ( this.encuestas['campos'][i].dependencias.some( v => this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value.indexOf(v) !== -1)
          || this.encuestas['campos'][i].dependencias.includes(this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value)) {
            if ( this.encuestas['campos'][i].value.length === 0 ) {
              contador_vacios++;
              preguntas_sin_responder += (i+1)+', ';
            }else{
              encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
              encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
            }
          }
        }

        //Seleccion Multiple
        if ( this.encuestas['campos'][i].tipo_pregunta === 'select_multiple [list_name]' && !this.encuestas['campos'][i].depende) {
          if ( this.encuestas['campos'][i].value.length === 0 ) {
            
            
            contador_vacios++;
            preguntas_sin_responder += (i+1)+', ';
          }else{
            encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
            encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
          }
        }

        //Departamento
        if ( this.encuestas['campos'][i].tipo_pregunta === 'depto' && !this.encuestas['campos'][i].depende) {
          if ( this.encuestas['campos'][i].value.length === 0 ) {
            contador_vacios++;
            preguntas_sin_responder += (i+1)+', ';
          }else{
            encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
            encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
          }
        }

        if ( this.encuestas['campos'][i].tipo_pregunta === 'depto' && this.encuestas['campos'][i].depende) {
          if ( this.encuestas['campos'][i].dependencias.some( v => this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value.indexOf(v) !== -1)
          || this.encuestas['campos'][i].dependencias.includes(this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value)) {
            if ( this.encuestas['campos'][i].value.length === 0 ) {
              contador_vacios++;
              preguntas_sin_responder += (i+1)+', ';
            }else{
              encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
              encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
            }
          }
        }

        

        //Municipio
        if ( this.encuestas['campos'][i].tipo_pregunta === 'muni' && this.encuestas['campos'][i].depende) {
          if (this.encuestas['campos'][this.dependencias.indexOf(this.encuestas['campos'][i].depende)].value) {
            if ( this.encuestas['campos'][i].value.length === 0 ) {
              contador_vacios++;
              preguntas_sin_responder += (i+1)+', ';
            }else{
              encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
              encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
            }
          }
         
        }

        // if ( this.encuestas['campos'][i].tipo_pregunta === 'select_multiple [list_name]' && this.encuestas['campos'][i].depende) {
        //   if ( this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value === this.encuestas['campos'][i].dependencia ) {
        //     if ( this.encuestas['campos'][i].value.length === 0 ) {
        //       contador_vacios++;
        //       preguntas_sin_responder += (i+1)+', ';
        //     }else{
        //       encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
        //       encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
        //     }
        //   }
        // }

        if ( this.encuestas['campos'][i].tipo_pregunta === 'select_multiple [list_name]' && this.encuestas['campos'][i].depende) {
          if ( this.encuestas['campos'][i].dependencias.some( v => this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value.indexOf(v) !== -1)
          || this.encuestas['campos'][i].dependencias.includes(this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value)) {
            if ( this.encuestas['campos'][i].value.length === 0 ) {
              contador_vacios++;
              preguntas_sin_responder += (i+1)+', ';
            }else{
              encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
              encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
            }
          }
        }

        //Termina seleccion multiple

        //Seleccion Multiple con detalle
        if ( this.encuestas['campos'][i].tipo_pregunta === 'select_multiple [detail]' && !this.encuestas['campos'][i].depende) {
          if ( this.encuestas['campos'][i].value.length === 0 ) {
            contador_vacios++;
            preguntas_sin_responder += (i+1)+', ';
          }else{
            encuesta[ this.encuestas['campos'][i].id ] = this.encuestas['campos'][i].value;
            encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
          }
        }

        if ( this.encuestas['campos'][i].tipo_pregunta === 'select_multiple [detail]' && this.encuestas['campos'][i].depende) {
          if ( this.encuestas['campos'][i].dependencias.some( v => this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value.indexOf(v) !== -1)
              || this.encuestas['campos'][i].dependencias.includes(this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value)) {
            if ( this.encuestas['campos'][i].value.length === 0 ) {
              contador_vacios++;
              preguntas_sin_responder += (i+1)+', ';
            }else{

              let details_arr = new Array();

              this.encuestas['campos'][i].value.forEach( (val: any) => {
                this.encuestas['campos'][i].respuestas.forEach( (res: any) => {
                  if ( val === res.respuesta ) {
                    
                    res.detalles.forEach( ( detalle:any) => {
                      if (detalle.value.length === 0) {
                        contador_vacios++;
                        preguntas_sin_responder += val+' Detalle, ';}
                      
                    });
                    
                    details_arr.push(res);
                   

                  }
                });
              });
            
              encuesta[ this.encuestas['campos'][i].id ] = details_arr;
            }
          }
        }
        //Termina seleccion multiple  con detalle
        
        // if ( this.encuestas['campos'][i].tipo_pregunta === 'geopoint' && !this.encuestas['campos'][i].depende) {
        //   if ( this.encuestas['campos'][i].x.length === 0 || this.encuestas['campos'][i].y.length === 0 ) {
        //     contador_vacios++;
        //     preguntas_sin_responder += (i+1)+', ';
        //   }else{
        //     encuesta[ 'x' ] = this.encuestas['campos'][i].x;
        //     encuesta[ 'y' ] = this.encuestas['campos'][i].y;
        //   }
        // }

        // if ( this.encuestas['campos'][i].tipo_pregunta === 'geopoint' && this.encuestas['campos'][i].depende) {
        //   if ( this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende)) ].value === this.encuestas['campos'][i].dependencia ) {
        //     if ( this.encuestas['campos'][i].x.length === 0 || this.encuestas['campos'][i].y.length === 0 ) {
        //       contador_vacios++;
        //       preguntas_sin_responder += (i+1)+', ';
        //     }else{
        //       encuesta[ 'x' ] = this.encuestas['campos'][i].x;
        //       encuesta[ 'y' ] = this.encuestas['campos'][i].y;
        //     }
        //   }
        // }


        // if ( this.encuestas['campos'][i].tipo_pregunta === 'image' && !this.encuestas['campos'][i].depende) {
        //   if ( this.encuestas['campos'][i].value.length === 0 ) {
        //     //contador_vacios++;
        //   }else{
        //     encuesta[ this.encuestas['campos'][i].campo ] = this.encuestas['campos'][i].value;
        //     encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
        //   }
        // }

        // if ( this.encuestas['campos'][i].tipo_pregunta === 'image' && this.encuestas['campos'][i].depende) {
        //   if ( this.encuestas['campos'][ this.dependencias.indexOf(parseInt(this.encuestas['campos'][i].depende))].value === this.encuestas['campos'][i].dependencia ) {
        //     if ( this.encuestas['campos'][i].value.length === 0 ) {
        //       //contador_vacios++;
        //     }else{
        //       encuesta[ this.encuestas['campos'][i].campo ] = this.encuestas['campos'][i].value;
        //       encuesta2[ this.encuestas['campos'][i].campo_id ] = this.encuestas['campos'][i].value;
        //     }
        //   }
        // }
    }
    

    if ( contador_vacios != 0 ) {

      loading.dismiss();

      this.alertCtrl.create({
        title: 'Error!!!!',
        subTitle: 'No debe dejar campos en blanco, revise los campos: ' + preguntas_sin_responder ,
        buttons: ['OK']
      }).present();
    }else if( !this.encuestas['x'] || !this.encuestas['y']){

      loading.dismiss();

      this.alertCtrl.create({
        title: 'Error!!!!',
        subTitle: 'Las coordenadas no deben de estar vacias ',
        buttons: ['OK']
      }).present();
    }else{

      encuesta[ 'encuesta_id' ] = this.encuestas['id'];
      encuesta['x'] = this.encuestas['x'];
      encuesta['y'] = this.encuestas['y']


      this._ep.guardarEncuesta( encuesta )
        .then( ( index:number ) => {

          this._ep.subirEncuestas( index, this._up.id_usuario )
            .then( resp => {

              loading.dismiss();

              if (resp) {
                this._ep.removeEncuesta( index );
                this.alertCtrl.create({
                  title: 'Éxito',
                  message: 'La encuesta ha sido subida con éxito',
                  buttons: [{
                    text: 'OK',
                    role: 'cancel',
                    handler: () => {
                      this.navCtrl.pop();
                    }
                  }]
                }).present();

              }else{
                this.alertCtrl.create({
                  title: 'Error',
                  message: 'La encuesta no se ha podido subir, se ha guardado en el teléfono',
                  buttons: [{
                    text: 'OK',
                    role: 'cancel',
                    handler: () => {
                      this.navCtrl.pop();
                    }
                  }]
                }).present();
              }
            });
        });
    }

  }

  cleanField( idx: number ){

    let cont = 0;

    if(Array.isArray(this.encuestas['campos'][this.dependencias.indexOf(this.encuestas['campos'][idx].depende)].value)){
      this.encuestas['campos'][this.dependencias.indexOf(this.encuestas['campos'][idx].depende)].value.forEach( (value: string) => {
        
        // if ( idx === 27) {
        //   console.log('Valor', value);
        //   console.log('Dependencias', this.encuestas['campos'][idx]['dependencias']);
        //   console.log('Es válido?', this.encuestas['campos'][idx]['dependencias'].includes(value));
        //   console.log('Longitud', this.encuestas['campos'][idx]['dependencias'].length);
          
          
          
        // }
    
        if (this.encuestas['campos'][idx]['dependencias'].includes(value)) {
          cont++; 
        }
      });
      
      // if ( idx === 27) {
      //   console.log('Antes',this.encuestas['campos'][idx]['value']);
      //   console.log('Antes',cont);
        
        
      // }

      if ( !(this.encuestas['campos'][idx]['dependencias'].length == cont) ) {
        
       
        this.encuestas['campos'][idx]['value']=''; 

        // console.log('Despues',this.encuestas['campos'][idx]['value']);
      }
    } else{
      if (!this.encuestas['campos'][idx]['dependencias'].includes(this.encuestas['campos'][this.dependencias.indexOf(this.encuestas['campos'][idx].depende)].value)) {
        this.encuestas['campos'][idx]['value']=''
      }
    }
  }

  cleanMunicipio( idx: number ){

    if (!this.encuestas['campos'][this.dependencias.indexOf(this.encuestas['campos'][idx].depende)].value) {
      this.encuestas['campos'][idx]['value']=''
    }
    
  }

}
