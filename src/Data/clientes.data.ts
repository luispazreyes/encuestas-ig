export const Clientes =  [
        {
            "id": "001-00002",
            "nombre": "CERVECERIA HONDUREÑA S.A. de C.V."
        },
        {
            "id": "001-00005",
            "nombre": "PARQUE INDUSTRIAL BUFALO, S.A."
        },
        {
            "id": "001-00029",
            "nombre": "CONSTRUCTORA WILLIAM & MOLINA S.A. de C.V."
        },
        {
            "id": "001-00031",
            "nombre": "PRODUCTORA DE AGREGADOS Y DERIVADOS DEL CONCRETO S.A. DE C.V."
        },
        {
            "id": "001-00033",
            "nombre": "EMPRESA DE MATERIALES ESTRUCTURALES PARA LA CONSTRUCCION S. de R.L."
        },
        {
            "id": "001-00038",
            "nombre": "ELCATEX S. de R.L. de C.V."
        },
        {
            "id": "001-00039",
            "nombre": "CONSTRUCTORA DELTA, S.A. de C.V."
        },
        {
            "id": "001-00040",
            "nombre": "CEMENTOS DEL NORTE, S.A."
        },
        {
            "id": "001-00051",
            "nombre": "DISTRIBUIDORA INDUSTRIAL (Cta.SPS-Lps.)"
        },
        {
            "id": "001-00074",
            "nombre": "PROFESIONALES DE LA CONSTRUCCION, S.A. de C.V."
        },
        {
            "id": "001-00075",
            "nombre": "TECNOLOGIA DE PROYECTOS, S. de R.L. de C.V."
        },
        {
            "id": "001-00091",
            "nombre": "AMERICAN PACIFIC HONDURAS S.A. de C.V."
        },
        {
            "id": "001-00100",
            "nombre": "EMPRESA DE CONSTRUCCION Y TRANSPORTE ETERNA, S.A. de C.V."
        },
        {
            "id": "001-00102",
            "nombre": "DURACRETO S.A."
        },
        {
            "id": "001-00117",
            "nombre": "CARACOL KNITS S.A. de C.V."
        },
        {
            "id": "001-00146",
            "nombre": "INMOBILIARIA HONDUREÑA DEL VALLE, S.A. de C.V."
        },
        {
            "id": "001-00177",
            "nombre": "TECPROFIRE S. de R.L. de C.V."
        },
        {
            "id": "001-00179",
            "nombre": "GRUPO \"J\", S.A."
        },
        {
            "id": "001-00187",
            "nombre": "INDUSTRIAS DEL METAL Y DE LA CONSTRUCCION S.A de C.V."
        },
        {
            "id": "001-00215",
            "nombre": "CORAL KNITS S.A. de C.V."
        },
        {
            "id": "001-00227",
            "nombre": "EMPRESA NACIONAL DE ENERGIA ELECTRICA"
        },
        {
            "id": "001-00261",
            "nombre": "STANDARD FRUIT DE HONDURAS"
        },
        {
            "id": "001-00348",
            "nombre": "TRANYCOP S.A. de C.V."
        },
        {
            "id": "001-00355",
            "nombre": "DESARROLLOS CONSTRUCTIVOS ARQUETIPO S. de R.L."
        },
        {
            "id": "001-00371",
            "nombre": "EMBOTELLADORA DE SULA S.A."
        },
        {
            "id": "001-00384",
            "nombre": "CONTRATISTAS, CONSULTORES DE LA CONSTRUCCION, S. DE R.L."
        },
        {
            "id": "001-00402",
            "nombre": "ENERGIA Y COMUNICACION S. de R.L."
        },
        {
            "id": "001-00417",
            "nombre": "INGENIERIA ELECTROMECANICA  Y CONSTRUCCION DE OBRAS CIVILES, S. de R.L. de C.V."
        },
        {
            "id": "001-00443",
            "nombre": "PERSPECTIVAS ARQUITECTONICAS"
        },
        {
            "id": "001-00513",
            "nombre": "FRIO PARTES S.A. de C.V."
        },
        {
            "id": "001-00557",
            "nombre": "MOLINO HARINERO SULA, S.A de C.V."
        },
        {
            "id": "001-00573",
            "nombre": "WILLIAMS & ASOCIADOS CONSTRUCTORES S.A de C.V."
        },
        {
            "id": "001-00588",
            "nombre": "CENTRO FERRETERO TORNIFESA S DE R.L. DE C.V."
        },
        {
            "id": "001-00591",
            "nombre": "FERRETERIA MONTERROSO, S.A. de C.V."
        },
        {
            "id": "001-00641",
            "nombre": "PREMIUN & CONFORT, S.A. de C.V."
        },
        {
            "id": "001-00652",
            "nombre": "LOTIFICADORA SULA S.A de C.V."
        },
        {
            "id": "001-00713",
            "nombre": "EMPRESA CONSTRUCTORA DIEK, S.A."
        },
        {
            "id": "001-00737",
            "nombre": "HORMIGON CONSTRUCTORA  S. de R.L."
        },
        {
            "id": "001-00738",
            "nombre": "FLORES & FLORES INGENIERIA S. de R.L."
        },
        {
            "id": "001-00794",
            "nombre": "ALMACEN FERRETERO SULA S.A. DE C.V."
        },
        {
            "id": "001-00881",
            "nombre": "CONSTRUCTORA OMAR ABUFELE"
        },
        {
            "id": "001-00919",
            "nombre": "ACCIONES, INMUEBLES Y VALORES, S.A. de C.V."
        },
        {
            "id": "001-00926",
            "nombre": "IMPORTADORA DE VEHICULOS S.A."
        },
        {
            "id": "001-00946",
            "nombre": "FABRICACIONES METALICAS MARADIAGA, S.DE R.L."
        },
        {
            "id": "001-01030",
            "nombre": "INVERSIONES NUEVOS HORIZONTES, S.A. de C.V."
        },
        {
            "id": "001-01130",
            "nombre": "HONDURAS CONSTRUCTION COMPANY"
        },
        {
            "id": "001-01167",
            "nombre": "HERRAMIENTAS LA ATLANTICA S. de R.L."
        },
        {
            "id": "001-01458",
            "nombre": "INGENIERIA BERLIOZ S. DE R.L DE C.V."
        },
        {
            "id": "001-01459",
            "nombre": "MUNICIPALIDAD DE CHOLOMA"
        },
        {
            "id": "001-01511",
            "nombre": "INMSA ARGO INTERNATIONAL S.A. de C.V."
        },
        {
            "id": "001-01588",
            "nombre": "EQUIPOS Y CONSTRUCCIONES S. DE R.L. de C.V."
        },
        {
            "id": "001-01683",
            "nombre": "UNIVERSIDAD TECNOLOGICA CENTROAMERICANA"
        },
        {
            "id": "001-01904",
            "nombre": "TALLER INDUSTRIAL SANZIO"
        },
        {
            "id": "001-02038",
            "nombre": "TECNOCOLOR PANAVISION S.A DE C.V."
        },
        {
            "id": "001-02059",
            "nombre": "SUPERMERCADOS JUNIOR"
        },
        {
            "id": "001-02139",
            "nombre": "PARTES SERVICIOS Y MONTACARGAS AGUILAR"
        },
        {
            "id": "001-02183",
            "nombre": "CONCESIONES ESPECIALIZADAS S.A. de C.V."
        },
        {
            "id": "001-02451",
            "nombre": "SUMINISTROS ELECTRICOS S. de R.L. de C.V."
        },
        {
            "id": "001-02589",
            "nombre": "CHIQUITA HONDURAS COMPANY LTD"
        },
        {
            "id": "001-02688",
            "nombre": "AYRE TEGUCIGALPA, S.A. de C.V."
        },
        {
            "id": "001-02820",
            "nombre": "FERRETERIA Y COMERCIO S.A DE C.V."
        },
        {
            "id": "001-02903",
            "nombre": "SUMINISTROS Y MONTAJES S. DE R.L. C.V."
        },
        {
            "id": "001-03293",
            "nombre": "ALUMINIOS COMERCIALES S.A. De C.V."
        },
        {
            "id": "001-03304",
            "nombre": "INGENIERIA SABILLON, S. de R.L."
        },
        {
            "id": "001-03605",
            "nombre": "INDUSTRIA CONCRETERA CENTROAMERICANA S.A. de C.V."
        },
        {
            "id": "001-03642",
            "nombre": "MULTI CONSTRUCCIONES, S.A. de C.V."
        },
        {
            "id": "001-03740",
            "nombre": "TODO FACIL FERRETERIA"
        },
        {
            "id": "001-04007",
            "nombre": "GILDAN ACTIVEWEAR HONDURAS TEXTILE COMPANY S. de R.L."
        },
        {
            "id": "001-04025",
            "nombre": "GILDAN HONDURAS HOSIERY FACTORY, S. de R.L."
        },
        {
            "id": "001-04033",
            "nombre": "GILDAN CHOLOMA TEXTILES, S. de R.L."
        },
        {
            "id": "001-04676",
            "nombre": "COMERCIAL Y FERRETERIA MILLENIUM S. DE R. L. DE C. V."
        },
        {
            "id": "001-04813",
            "nombre": "ENERGIA RENOVABLE S.A. de C.V."
        },
        {
            "id": "001-04816",
            "nombre": "CEIBA TEXTILES S. de R.L."
        },
        {
            "id": "001-04882",
            "nombre": "ZONA LIBRE GREEN VALLEY INDUSTRIAL PARK S.A. DE C.V."
        },
        {
            "id": "001-04884",
            "nombre": "TECHINICAL SYSTEMS DISTRIBUTORS S.A."
        },
        {
            "id": "001-04890",
            "nombre": "AGUAS DE SAN PEDRO S.A. DE C.V."
        },
        {
            "id": "001-04904",
            "nombre": "CONSTRUCTORA LOPEZ RIVERA"
        },
        {
            "id": "001-04914",
            "nombre": "LECHE Y DERIVADOS S.A."
        },
        {
            "id": "001-04917",
            "nombre": "BROTHERS AUTO PARTS S DE R.L(CTA. SPS-Lps.)"
        },
        {
            "id": "001-04919",
            "nombre": "AGENCIA LA MUNDIAL, S.A. de C.V."
        },
        {
            "id": "001-04921",
            "nombre": "INGENIERIA Y DESARROLLOS S.A."
        },
        {
            "id": "001-04935",
            "nombre": "INCAL S.A. de C.V."
        },
        {
            "id": "001-04936",
            "nombre": "TODO EN CONCRETO HONDURAS, S. de R.L. de C.V."
        },
        {
            "id": "001-04938",
            "nombre": "ELECTROTECNIA S.A de C.V."
        },
        {
            "id": "001-04952",
            "nombre": "BLINDAJES S.A DE C.V."
        },
        {
            "id": "001-04958",
            "nombre": "CONCRETOS Y AGREGADOS DE SULA S.A -CYASSA (CTA.SPS-Lps.)"
        },
        {
            "id": "001-04961",
            "nombre": "SERVICIOS DE MANTENIMIENTO Y CONSTRUCCIÓN S. A. DE C. V."
        },
        {
            "id": "001-04966",
            "nombre": "INDUSTRIAS CONTEMPO S.A. de C.V."
        },
        {
            "id": "001-04973",
            "nombre": "CORPORACION DINANT S.A. de C.V."
        },
        {
            "id": "001-04976",
            "nombre": "ENERGIA Y TRANSMISION S.A. de C.V."
        },
        {
            "id": "001-04980",
            "nombre": "BODEGAS Y NAVES INDUSTRIALES S.A. DE C.V"
        },
        {
            "id": "001-04983",
            "nombre": "SERVICIOS PARA EL DESARROLLO DE LA CONSTRUCCION S. de R. L. de C.V."
        },
        {
            "id": "001-04984",
            "nombre": "SEABOARD HONDURAS S. DE RL DE CV"
        },
        {
            "id": "001-04985",
            "nombre": "CORDON´S HEAVY EQUIPMENT S. de R.L."
        },
        {
            "id": "001-04990",
            "nombre": "MEXICHEM HONDURAS S.A. DE C.V."
        },
        {
            "id": "001-04991",
            "nombre": "RLA MANUFACTURING, S. de R.L."
        },
        {
            "id": "001-04997",
            "nombre": "INGENIEROS CONSULTORES Y CONSTRUCTORES ELECTROMECANICOS  S.A. de C.V."
        },
        {
            "id": "001-05006",
            "nombre": "TECNOSUPPLIER ( CTA.LPS-SPS)"
        },
        {
            "id": "001-05008",
            "nombre": "GILDAN HONDURAS PROPERTIES S.DE. R.L."
        },
        {
            "id": "001-05009",
            "nombre": "FABRICA DE EXTRACTORES FUENTES"
        },
        {
            "id": "001-05010",
            "nombre": "METALIZACION INDUSTRIAL CARGUS S. R.L. de C.V."
        },
        {
            "id": "001-050601",
            "nombre": "INVERSIONES AMALGAMADAS S.A DE .C.V."
        },
        {
            "id": "001-05066",
            "nombre": "GENERACION DE ENERGIA RENOVABLE S.A DE C.V."
        },
        {
            "id": "001-05070",
            "nombre": "MARKETING Y PUBLICIDAD S.A."
        },
        {
            "id": "001-05078",
            "nombre": "SERVICIOS Y SUMINISTROS PARA LA CONSTRUCCION, S.A. de C.V."
        },
        {
            "id": "001-05099",
            "nombre": "CONSTRUCCIONES EL ROBLE"
        },
        {
            "id": "001-05147",
            "nombre": "RCN INGENIERIA"
        },
        {
            "id": "001-05152",
            "nombre": "INVERSIONES HONDUREÑO ARABE S.A de C.V."
        },
        {
            "id": "001-05160",
            "nombre": "SOLUCIONES ARQUITECTONICAS S.A DE C.V."
        },
        {
            "id": "001-05169",
            "nombre": "AYRE DE HONDURAS(CTA.LPS-SPS)"
        },
        {
            "id": "001-05170",
            "nombre": "CONSTRUCCIONES MARTE S.A de C.V."
        },
        {
            "id": "001-05179",
            "nombre": "OLEOPRODUCTOS DE HONDURAS S.A DE C.V."
        },
        {
            "id": "001-05201",
            "nombre": "AGRICOLA TORNABE, S.A."
        },
        {
            "id": "001-05214",
            "nombre": "PROCESADORA GUANGOLOLA DE CARNES S.A. DE C.V."
        },
        {
            "id": "001-05231",
            "nombre": "HIDROELECTRICA EL VOLCÁN, S.A. de C.V."
        },
        {
            "id": "001-05233",
            "nombre": "BLUE ENERGY, S.A. de C.V,"
        },
        {
            "id": "001-05242",
            "nombre": "ALTIA BUSINESS PARK, S.A. de C.V."
        },
        {
            "id": "001-05245",
            "nombre": "TECNISERVICIOS"
        },
        {
            "id": "001-05248",
            "nombre": "PROYECTO DE ORDENAMIENTO TERRITORIAL COMUNAL Y PROTECCION AL MEDIO AMBIENTE EN RIO PLATANO"
        },
        {
            "id": "001-05255",
            "nombre": "CONTRATISTAS NACIONALES DE CONSTRUCCION SA DE CV"
        },
        {
            "id": "001-05256",
            "nombre": "BANEGAS HILL & ASOCIADOS"
        },
        {
            "id": "001-05257",
            "nombre": "BIJAO ELECTRIC COMPANY, S.A. de C.V."
        },
        {
            "id": "001-05264",
            "nombre": "URBANIZADORA NACIONAL S.A DE C.V."
        },
        {
            "id": "001-05265",
            "nombre": "INMOBILIARIA AMERICANA S.A DE C.V."
        },
        {
            "id": "001-05267",
            "nombre": "ACEROS ALFA, S.A."
        },
        {
            "id": "001-05269",
            "nombre": "CONSTRUCTORA INGENIERIA CENTROAMERICANA, S.A. de C.V."
        },
        {
            "id": "001-05273",
            "nombre": "HONDURAN GREEN POWER CORPORATION,S.A. de C.V."
        },
        {
            "id": "001-05274",
            "nombre": "GLOBAL LOGISTICS, S.A. DE C.V."
        },
        {
            "id": "001-05275",
            "nombre": "CONSTRUCTORA EMCO S.A. de C.V."
        },
        {
            "id": "001-05276",
            "nombre": "GILDAN HONDURAS TRADING S. de R.L."
        },
        {
            "id": "001-05277",
            "nombre": "EXPO PIEDRAS S.A. de C.V."
        },
        {
            "id": "001-05278",
            "nombre": "ZIP EL PORVENIR, S.A."
        },
        {
            "id": "001-05279",
            "nombre": "EMPRESA DE INVERSIONES Y SERVICIOS MULTIPLES ZUNIGA S. de R.L. de C.V."
        },
        {
            "id": "001-05280",
            "nombre": "INGNOVARQ, S.A. de C.V."
        },
        {
            "id": "001-05281",
            "nombre": "CONSTRUCTORA AGUILAR REYES Y ASOCIADOS, S. de R.L. de C.V."
        },
        {
            "id": "001-05285",
            "nombre": "AKH S. de R.L."
        },
        {
            "id": "001-05286",
            "nombre": "SERVICIOS INTEROCEANICOS, S.A. de C.V."
        },
        {
            "id": "001-05287",
            "nombre": "N & M CONSTRUCCIONES S. de R.L. de C.V."
        },
        {
            "id": "001-05292",
            "nombre": "INGENIERIA DE CARRETERAS S.A"
        },
        {
            "id": "001-05296",
            "nombre": "NUEVA SOCIEDAD HOTELERA"
        },
        {
            "id": "001-05297",
            "nombre": "VIDRIOS TRANSFORMADOS, S.A. de C.V."
        },
        {
            "id": "001-05298",
            "nombre": "CONSTRUCCIONES CERRATO Y ASOCIADOS S. de R.L. de C.V."
        },
        {
            "id": "001-05300",
            "nombre": "INGENIERIA GERENCIAL, S.A. de C.V."
        },
        {
            "id": "001-05301",
            "nombre": "MONOLIT DE HONDURAS S.A. de C.V."
        },
        {
            "id": "001-05306",
            "nombre": "INVERSIONES ALIADAS, S.A. de C.V."
        },
        {
            "id": "001-05308",
            "nombre": "VIENTOS DE SAN MARCOS, S.A. de C.V."
        },
        {
            "id": "001-05309",
            "nombre": "TIENDAS CARRION S.A de C.V."
        },
        {
            "id": "001-05312",
            "nombre": "SERCOVIM S. DE R.L. DE C.V."
        },
        {
            "id": "001-05315",
            "nombre": "GEO STRUCTURES, S.A."
        },
        {
            "id": "001-05317",
            "nombre": "PROFESIONALES EN LOGISTICA, S.A. DE C.V."
        },
        {
            "id": "001-05324",
            "nombre": "DESARROLLOS, S.A. de C.V."
        },
        {
            "id": "001-05326",
            "nombre": "INVERSIONES PENTA, S.A."
        },
        {
            "id": "001-05329",
            "nombre": "HIDROELECTRICA CUYAGUAL, S.A. de C.V."
        },
        {
            "id": "001-05331",
            "nombre": "AMERICA ALUMINIO Y VIDRIO, S. de R.L."
        },
        {
            "id": "001-05338",
            "nombre": "CONCESIONARIA VIAL HONDURAS, S.A. de C.V."
        },
        {
            "id": "001-05347",
            "nombre": "SUPER FARMACIA SIMAN, S.A."
        },
        {
            "id": "001-05351",
            "nombre": "GILDAN MAYAN TEXTILES S. DE R.L"
        },
        {
            "id": "001-05352",
            "nombre": "ABARROTERIA EL SUPER BARATO"
        },
        {
            "id": "001-05357",
            "nombre": "SERVICIOS INCO, S. de R. L. DE C V"
        },
        {
            "id": "001-05362",
            "nombre": "COATS HONDURAS"
        },
        {
            "id": "001-05366",
            "nombre": "ICECSA"
        },
        {
            "id": "001-05369",
            "nombre": "INGENIERIA CODIMEL S. DE R.L DE C.V"
        },
        {
            "id": "001-05376",
            "nombre": "RAMOS DENIS"
        },
        {
            "id": "001-05378",
            "nombre": "FERREMAS S. DE R.L"
        },
        {
            "id": "001-05379",
            "nombre": "EDINCO S. DE R.L"
        },
        {
            "id": "001-05386",
            "nombre": "INVERSIONES MCLIBERTY SANCHEZ SRL DE CF"
        },
        {
            "id": "001-05396",
            "nombre": "EXTRUM, S.A. de C.V."
        },
        {
            "id": "001-05407",
            "nombre": "SERVICIOS Y TERMINALES SA DE CV"
        },
        {
            "id": "001-05416",
            "nombre": "INGENIERIA Y SUMINISTRO DE MATERIALES"
        },
        {
            "id": "001-05421",
            "nombre": "TORRES ARQUITECTO S de R.L."
        },
        {
            "id": "001-05423",
            "nombre": "BAUTISTA HUMBERTO"
        },
        {
            "id": "001-05425",
            "nombre": "FERRETERIA TODOS"
        },
        {
            "id": "001-05433",
            "nombre": "FUNDACION ESCUELA PARA LOS NIÑOS DEL MUNDO HONDURAS"
        },
        {
            "id": "001-05438",
            "nombre": "CONSTRUCTORA PALADA S.A"
        },
        {
            "id": "001-05444",
            "nombre": "FERRETERIA EL MIRADOR, S. de R.L."
        },
        {
            "id": "001-05449",
            "nombre": "HOGARES MODERNOS Y EXCLUSIVOS DE HONDURAS SA"
        },
        {
            "id": "001-05453",
            "nombre": "DICONSET S.A DE C.V"
        },
        {
            "id": "001-05468",
            "nombre": "DIAZ VASQUEZ DENIS DAVID"
        },
        {
            "id": "001-05469",
            "nombre": "PANIFICADORA LA POPULAR"
        },
        {
            "id": "001-05471",
            "nombre": "GRUPO AG INVERSIONES"
        },
        {
            "id": "001-05474",
            "nombre": "PEREIRA BENNETT ROLANDO"
        },
        {
            "id": "001-05483",
            "nombre": "CONSTRUCTORA OCASA S. DE R.L."
        },
        {
            "id": "001-05501",
            "nombre": "DPROYECTOS"
        },
        {
            "id": "001-05512",
            "nombre": "CODECON S.A. DE C.V"
        },
        {
            "id": "001-05514",
            "nombre": "INDUMECA"
        },
        {
            "id": "001-05533",
            "nombre": "CONSTRUCTORA BUELTO ASOCIADOS"
        },
        {
            "id": "001-05539",
            "nombre": "DISTRIBUCIONES MULTINACIONALES S.A."
        },
        {
            "id": "001-05540",
            "nombre": "ARC CONSTRUCCION Y ADMINISTRACION DE PROYECTOS"
        },
        {
            "id": "001-05545",
            "nombre": "MUNICIPALIDAD DE GRACIAS, LEMPIRA"
        },
        {
            "id": "001-05550",
            "nombre": "CONSTRUCTORA FORTALEZA DE HONDURAS S DE RL DE CV"
        },
        {
            "id": "001-05552",
            "nombre": "ALVARADO GAMEZ LUIS"
        },
        {
            "id": "001-05556",
            "nombre": "ICONDEPRO"
        },
        {
            "id": "001-05561",
            "nombre": "HUGO COELLO ARQUITECTOS"
        },
        {
            "id": "001-05588",
            "nombre": "TRANSPORTES ILANGUEÑOS SRL CV"
        },
        {
            "id": "001-05591",
            "nombre": "GRUPO DIDECO"
        },
        {
            "id": "001-05625",
            "nombre": "CONDOMINIOS DEL VALLE, S.A. de C.V."
        },
        {
            "id": "001-05629",
            "nombre": "PROVAC"
        },
        {
            "id": "001-05652",
            "nombre": "HOTEL VALENCIA"
        },
        {
            "id": "001-05656",
            "nombre": "CONSTRUCTORA ULLOA"
        },
        {
            "id": "001-05670",
            "nombre": "SEL STORE"
        },
        {
            "id": "001-05674",
            "nombre": "SHEKINAH S.A DE C.F"
        },
        {
            "id": "001-05675",
            "nombre": "HIDROELECTRICA DE OCCIDENTE S. DE R.L."
        },
        {
            "id": "001-05680",
            "nombre": "GRUPO DISER - B"
        },
        {
            "id": "001-05706",
            "nombre": "INCOBE"
        },
        {
            "id": "001-05717",
            "nombre": "INGENIUM"
        },
        {
            "id": "001-05753",
            "nombre": "CONSTRUCTORA ROCASOL S. DE R.L."
        },
        {
            "id": "001-05757",
            "nombre": "MARADIAGA PUERTO LUIS ANTONIO"
        },
        {
            "id": "001-05760",
            "nombre": "INVERCOSSADU"
        },
        {
            "id": "001-05805",
            "nombre": "FERRETERIA EL CARMEN"
        },
        {
            "id": "001-05808",
            "nombre": "COMPAÑIA AZUCARERA HONDUREÑA S.A."
        },
        {
            "id": "001-05840",
            "nombre": "DEINCO"
        },
        {
            "id": "001-05848",
            "nombre": "ISLAND CONCRETE"
        },
        {
            "id": "001-05871",
            "nombre": "INVERSIONES CALDERON Y ASOCIADOS"
        },
        {
            "id": "001-05881",
            "nombre": "DISTRIBUIDORA HECTOR GODOY"
        },
        {
            "id": "001-05905",
            "nombre": "VIERNES DE HONDURAS"
        },
        {
            "id": "001-05906",
            "nombre": "SICON"
        },
        {
            "id": "001-05924",
            "nombre": "SOCIEDAD ELECTRICA MESOAMERICANA S.A."
        },
        {
            "id": "001-05925",
            "nombre": "CONSTRUCTORA MONTECRISTO"
        },
        {
            "id": "001-05950",
            "nombre": "CONSTRUCTORA PONCE MARTINEZ S.R.L DE C.V"
        },
        {
            "id": "001-05978",
            "nombre": "VALLEJO VELASQUEZ NORMAN EDUARDO"
        },
        {
            "id": "001-05980",
            "nombre": "EDICOM S. DE R.L."
        },
        {
            "id": "001-05982",
            "nombre": "MISIONES DEL AGUA INTERNACIONAL"
        },
        {
            "id": "001-05993",
            "nombre": "PINARES CONSTRUCCION"
        },
        {
            "id": "001-05998",
            "nombre": "INVERSIONES COMERCIALES Y DESARROLLOS S.A. DE C.V."
        },
        {
            "id": "001-06000",
            "nombre": "MEJIA PETIT PERFECTO JESUS"
        },
        {
            "id": "001-06003",
            "nombre": "SERVICIOS ELECTROMECANICOS S. DE R.L. DE C.V."
        },
        {
            "id": "001-06018",
            "nombre": "DIPROCOM"
        },
        {
            "id": "001-06028",
            "nombre": "COINKA S DE RL"
        },
        {
            "id": "001-06055",
            "nombre": "MULTISERVICIOS FAJARDO S DE R.L"
        },
        {
            "id": "001-06065",
            "nombre": "MULTISERVICIOS FAJARDO S DE R.L."
        },
        {
            "id": "001-06099",
            "nombre": "FERRETERIA LEMPIRA"
        },
        {
            "id": "001-06104",
            "nombre": "INDUSTRIAS PINGUINO S. DE R.L. DE S.V."
        },
        {
            "id": "001-06115",
            "nombre": "GRUPO RIO S DE R.L."
        },
        {
            "id": "001-06135",
            "nombre": "COHDYL"
        },
        {
            "id": "001-06167",
            "nombre": "TODO CONCRETO SERVIS S DE RL"
        },
        {
            "id": "001-06189",
            "nombre": "SERVICIOS MULTIPLES MARTINEZ"
        },
        {
            "id": "001-06214",
            "nombre": "HIDROELECTRICA CENTRALES EL PROGRESO S.A DE C.V"
        },
        {
            "id": "001-06216",
            "nombre": "INMOBILIARIA LA COLINA"
        },
        {
            "id": "001-06218",
            "nombre": "HOTEL VERANDA"
        },
        {
            "id": "001-06223",
            "nombre": "SORTO MEJIA JOSE ALFREDO"
        },
        {
            "id": "001-06241",
            "nombre": "SERVICIO DE INFORMACION GEOGRAFICA MERCANTIL S.A. DE C.V"
        },
        {
            "id": "001-06263",
            "nombre": "GARCIA & GARCIA COMPAÑIA SERVICIOS S DE RL"
        },
        {
            "id": "001-06283",
            "nombre": "ELEMENTOS DECORATIVOS"
        },
        {
            "id": "001-06293",
            "nombre": "CONSTRUCCIONES CIVILES AGAPE"
        },
        {
            "id": "001-06303",
            "nombre": "ALUMINIOS Y VIDRIOS MARANATHA"
        },
        {
            "id": "001-06314",
            "nombre": "INTEGRA S DE R.L."
        },
        {
            "id": "001-06328",
            "nombre": "ZONA LIBRE HONDURAS S.A"
        },
        {
            "id": "001-06373",
            "nombre": "INTERMODA S.A DE C.V"
        },
        {
            "id": "001-06384",
            "nombre": "ARTISAM"
        },
        {
            "id": "001-06482",
            "nombre": "INVERSIONES CARBAJAL, S. DE R.L. DE C.V."
        },
        {
            "id": "001-06531",
            "nombre": "RC MULTISERVICIOS"
        },
        {
            "id": "001-06534",
            "nombre": "SERVICIOS CONSOLIDADOS DE HONDURAS"
        },
        {
            "id": "001-06559",
            "nombre": "CENTRO COMERCIAL LOS CASTAÑOS"
        },
        {
            "id": "001-06562",
            "nombre": "INTERMODAL OPERATORS S.A. DE C.V."
        },
        {
            "id": "001-06572",
            "nombre": "CENTRAL AMERICA TRAILER REPAIRS S. DE R.L. DE C.V."
        },
        {
            "id": "001-06596",
            "nombre": "GAMONEDA TURCIOS IRVIN"
        },
        {
            "id": "001-06640",
            "nombre": "MUNGUIA JULIO CESAR"
        },
        {
            "id": "001-06646",
            "nombre": "EMPACADORA 2000"
        },
        {
            "id": "001-06716",
            "nombre": "VIVAS AUTOMOTRIS"
        },
        {
            "id": "001-06719",
            "nombre": "LUBRISULA"
        },
        {
            "id": "001-06743",
            "nombre": "CENTRAL DE MANGUERAS"
        },
        {
            "id": "001-06749",
            "nombre": "CONSORCIO DURACRETO SANTOS"
        },
        {
            "id": "001-06768",
            "nombre": "INVERSIONES ESPECIALIZADAS CANAAN"
        },
        {
            "id": "001-06782",
            "nombre": "INGEMET"
        },
        {
            "id": "001-06785",
            "nombre": "INVERSIONES CASTELL"
        },
        {
            "id": "001-06798",
            "nombre": "BIENES Y RAICES FLORES"
        },
        {
            "id": "001-06808",
            "nombre": "A&A POSTENSADOS"
        },
        {
            "id": "001-06848",
            "nombre": "ABBOTT NELSON"
        },
        {
            "id": "001-06895",
            "nombre": "ALFARO AMAYA ISAIAS JOSUE"
        },
        {
            "id": "001-06919",
            "nombre": "GILDAN ACTIVEWEAR VILLANUEVA S.A."
        },
        {
            "id": "001-06983",
            "nombre": "DUARTE ALLAN JOEL"
        },
        {
            "id": "001-07002",
            "nombre": "DESARROLLOS HOTELEROS C/JDE HONDURAS-SPS,S.A. DE C.V."
        },
        {
            "id": "001-07057",
            "nombre": "ZIP CALPULES S.A."
        },
        {
            "id": "001-07061",
            "nombre": "ARRIOLA COREA JOSE MARIA"
        },
        {
            "id": "001-07099",
            "nombre": "ENERGIAS LIMPIAS DEL YAGUALA S.A DE CV"
        },
        {
            "id": "001-07105",
            "nombre": "BENEFICIO DE ARROZ PROGRESO SA DE CV"
        },
        {
            "id": "001-07115",
            "nombre": "INVERCOM"
        },
        {
            "id": "001-07160",
            "nombre": "HUEZO INDUSTRIAL S.A DE C.V."
        },
        {
            "id": "001-07183",
            "nombre": "VENTURA PEDRO ANGEL"
        },
        {
            "id": "001-07200",
            "nombre": "CONTRATISTAS ELECTROMECANICOS  S. DE R.L."
        },
        {
            "id": "001-07210",
            "nombre": "VIBRO BLOCK S DE R L DE C V ,"
        },
        {
            "id": "001-07213",
            "nombre": "CONSTRUCTORA SERRANO COLINDRES Y ASOCIADOS S. de R.L."
        },
        {
            "id": "001-07216",
            "nombre": "CID. S. DE R.L."
        },
        {
            "id": "001-07232",
            "nombre": "VARGAS CANALES Y ASOCIADOS SRL DE CV ,"
        },
        {
            "id": "001-07293",
            "nombre": "MARTA LIZETTE ZELAYA MEDRANO"
        },
        {
            "id": "001-07313",
            "nombre": "BERLIOZ AGUILERA JOSE EDUARDO"
        },
        {
            "id": "001-07316",
            "nombre": "INDUSTRIA SULA S. DE R.L."
        },
        {
            "id": "001-07321",
            "nombre": "UNIVERSIDAD CATOLICA DE HONDURAS NUESTRA SEÑORA REINA DE LA PAZ"
        },
        {
            "id": "001-07323",
            "nombre": "CONSTRUCTORA LA ESPAÑOLA S DE RL DE CV"
        },
        {
            "id": "001-07368",
            "nombre": "PRIDE PERFORMANCE FABRICS S A DE C V ,"
        },
        {
            "id": "001-07379",
            "nombre": "CHINA HARBOUR ENGINEERING COMPAN Y HONDURAS SA DE CV"
        },
        {
            "id": "001-07404",
            "nombre": "ACCESORIOS Y EQUIPOS ELECTRICOS S DE R.L."
        },
        {
            "id": "001-07415",
            "nombre": "INVERSIONES METROPARK S DE R.L DE CV"
        },
        {
            "id": "001-07440",
            "nombre": "CONSTRUCTORA CASTRO S DE R L."
        },
        {
            "id": "001-07466",
            "nombre": "CENTRO DERMATOLOGICO INTEGRAL SOCIEDAD DE RESPONSABILIDAD LIMITADA"
        },
        {
            "id": "001-07489",
            "nombre": "SERVICIOS INGENIERIA Y CONSTRUCCION GALEL HERNANDEZ ROMERO SRL DE CV"
        },
        {
            "id": "001-07577",
            "nombre": "INMOBILIARIA DEL NORTE S.A."
        },
        {
            "id": "001-07590",
            "nombre": "COMISARIATO LOS ANDES SA DE CV"
        },
        {
            "id": "001-07624",
            "nombre": "INDUSTRIAS METALICAS Y TECHOS SRL DE CV"
        },
        {
            "id": "001-07625",
            "nombre": "GALDAMEZ TOME SUYAPA VICTORIA"
        },
        {
            "id": "001-07633",
            "nombre": "SACOS AGRO-INDUSTRIALES"
        },
        {
            "id": "001-07653",
            "nombre": "WINGERS, S. DE R. L."
        },
        {
            "id": "001-07662",
            "nombre": "IIA TECNOLOGÍAS ESPECIALIZADAS S.A."
        },
        {
            "id": "001-07689",
            "nombre": "PINEDA CASTELLANOS GASPAR ANTONIO"
        },
        {
            "id": "001-07738",
            "nombre": "EDIFICA INMOBILIARIA S A DE C V"
        },
        {
            "id": "001-07747",
            "nombre": "GINA AVISAG, RECARTE LARA"
        },
        {
            "id": "001-07878",
            "nombre": "PERDOMO PERDOMO ARLEN DENIS"
        },
        {
            "id": "001-07922",
            "nombre": "TROCHEZ FERNANDEZ JS POMPEYO"
        },
        {
            "id": "001-07951",
            "nombre": "LARACH LARACH PEDRO"
        },
        {
            "id": "001-07994",
            "nombre": "MANCOMUNIDAD DE LOS MUNICIPIOS DEL CENTRO DE ATLANTIDA"
        },
        {
            "id": "001-08023",
            "nombre": "MUNDIOFERTAS SA DE CV"
        },
        {
            "id": "001-08034",
            "nombre": "HILOS Y MECHAS SA DE CV"
        },
        {
            "id": "001-08039",
            "nombre": "STASSANO RAQUEL ANGELA MARIA"
        },
        {
            "id": "001-08041",
            "nombre": "EMPRESA CONSTRUCTORA HENRIQUEZ"
        },
        {
            "id": "001-08048",
            "nombre": "OPCIONES CONSTRUCTIVAS S DE  R. L."
        },
        {
            "id": "001-08107",
            "nombre": "AVENDAÑO RODRIGUEZ JOKSON ESAU"
        },
        {
            "id": "001-08112",
            "nombre": "DESARROLLOS HOTELEROS DE HONDURAS SA DE CV"
        },
        {
            "id": "001-08121",
            "nombre": "INVERSIONES LOS PINARES S.A. DE C.V."
        },
        {
            "id": "001-08130",
            "nombre": "BOLIVAR TRADING-HONDURAS S DE R L"
        },
        {
            "id": "001-08145",
            "nombre": "FIX ING MANTENIMIENTO INGENIERIA S. DE R.L."
        },
        {
            "id": "001-08168",
            "nombre": "PERDOMO VASQUEZ EDWING ALEXANDER"
        },
        {
            "id": "001-08218",
            "nombre": "PINEDA PAREDES GASPAR ANTONIO"
        },
        {
            "id": "001-08220",
            "nombre": "SPORT CITY SA DE CV"
        },
        {
            "id": "001-08221",
            "nombre": "BARRALAGA PAZ CLAUDIA ALEJANDRA"
        },
        {
            "id": "001-08331",
            "nombre": "DISTRIBUIDORA ROSY S. DE R.L. DE C.V."
        },
        {
            "id": "001-08341",
            "nombre": "CONSTRUCTORA GESTION DE PROYECTOS SUCRE SA DE CV ,"
        },
        {
            "id": "001-08346",
            "nombre": "CONSTRUCT BUELTO Y ASOCIADOS SRL CV"
        },
        {
            "id": "001-08348",
            "nombre": "FULL ENGINEERING S DE R L DE CV"
        },
        {
            "id": "001-08349",
            "nombre": "ARISTA DE HONDURAS, S.A. DE C.V."
        },
        {
            "id": "001-08426",
            "nombre": "CONSTRUCOES GABRIEL A. S. COUTO S A"
        },
        {
            "id": "001-08433",
            "nombre": "OAC CORPORACION DE CONSTRUCCION ACCION, S. DE R.L. DE C.V."
        },
        {
            "id": "001-08448",
            "nombre": "BALEADAS EXPRESS S de R.L."
        },
        {
            "id": "001-08451",
            "nombre": "ULLOA Y ASOCIADOS S DE R L"
        },
        {
            "id": "001-08498",
            "nombre": "DIAZ REYES GUSTAVO ADOLFO"
        },
        {
            "id": "001-08508",
            "nombre": "GLOBAL MAINTENANCE SERVICES S.A DE C.V."
        },
        {
            "id": "001-08520",
            "nombre": "PALMEROLA INTERNATIONAL AIRPORT S A DE C V"
        },
        {
            "id": "001-08542",
            "nombre": "CASCADE CAPITAL GROUP INVESTMENTS S.A. DE C.V."
        },
        {
            "id": "001-08588",
            "nombre": "INCOBE FUELS S DE RL DE CV"
        },
        {
            "id": "001-08594",
            "nombre": "PROYECTOS Y SERVICIOS MULTIPLES EN CONSTRUCCION S DE RL DE CV"
        },
        {
            "id": "001-08612",
            "nombre": "RENTA Y VENTAS DE EQUIPO DE CONSTRUCCION S DE RL"
        },
        {
            "id": "001-08622",
            "nombre": "WONG ALVAREZ HEAVY PARTS SA DE CV"
        },
        {
            "id": "001-08637",
            "nombre": "HAWITH BUESO ALLAN GARY"
        },
        {
            "id": "001-08723",
            "nombre": "ELECEN S. A DE C.V."
        },
        {
            "id": "001-08728",
            "nombre": "YUJA ESCALANTE  IVAN SALOMON"
        },
        {
            "id": "001-08745",
            "nombre": "INDUSTRIAS PACER SA DE CV"
        },
        {
            "id": "001-08761",
            "nombre": "INTERIORS S.R.L DE CV"
        },
        {
            "id": "001-08800",
            "nombre": "SOCIEDAD UNIDA S DE R L DE C V"
        },
        {
            "id": "001-08816",
            "nombre": "CONSTRUCCIONES EN ACERO Y OBRAS CIVILES S.A."
        },
        {
            "id": "001-08830",
            "nombre": "CONSORCIO COPAN"
        },
        {
            "id": "001-08831",
            "nombre": "JEER MEJIA CARLOS ELIAS"
        },
        {
            "id": "001-08847",
            "nombre": "DNSG SRL"
        },
        {
            "id": "001-08897",
            "nombre": "TECH CENTER SRL"
        },
        {
            "id": "001-08940",
            "nombre": "NUÑEZ SOLORZANO GERSON JOSUE"
        },
        {
            "id": "001-08941",
            "nombre": "AGRICOLA MIRALDA S.A"
        },
        {
            "id": "001-08949",
            "nombre": "MADE IN CHINA SA DE CV"
        },
        {
            "id": "001-08985",
            "nombre": "GONZALES ORELLANA HERMES ULISES"
        },
        {
            "id": "001-08990",
            "nombre": "GILDAN TEXTILES DE SULA S DE R.L."
        },
        {
            "id": "001-09003",
            "nombre": "HERNANDEZ IZAGUIRRE ALEJANDRO JOSUE"
        },
        {
            "id": "001-09012",
            "nombre": "INVERSIONES INMOBILIARIAS DEL VALLE SA DE CV"
        },
        {
            "id": "001-09013",
            "nombre": "ZONA INDUSTRIAL DE EXPORTACION CHOLOMA S.A."
        },
        {
            "id": "001-09017",
            "nombre": "AUTO REPUESTOS BIGOTE SRL DE CV"
        },
        {
            "id": "001-09023",
            "nombre": "MONTACARGAS, S.A. DE C.V."
        },
        {
            "id": "001-09025",
            "nombre": "GASES DEL CARIBE SA"
        },
        {
            "id": "001-09029",
            "nombre": "JL PLANIFICACION"
        },
        {
            "id": "001-09050",
            "nombre": "INDUSTRIAS DEL CONCRETO S.A. DE C.V."
        },
        {
            "id": "001-09095",
            "nombre": "IRIAS BARDALES MARCO AURELIO"
        },
        {
            "id": "001-09118",
            "nombre": "OPTIMAL BUILD"
        },
        {
            "id": "001-09131",
            "nombre": "HOTEL CASA LA CORDILLERA SA DE CV"
        },
        {
            "id": "001-09132",
            "nombre": "INMOBILIARIA CLARITA SA DE CV"
        },
        {
            "id": "001-09139",
            "nombre": "ECO ENERGY S DE R L"
        },
        {
            "id": "001-09141",
            "nombre": "ORGANIZACION HOGAR DE LA PROMESA"
        },
        {
            "id": "001-09153",
            "nombre": "STOPS YONKER ONE SRL DE CV"
        },
        {
            "id": "001-09156",
            "nombre": "BENDEK TURCIOS MARBELLA"
        },
        {
            "id": "001-09157",
            "nombre": "CENTRO CULTURAL SAMPEDRANO"
        },
        {
            "id": "001-09158",
            "nombre": "ENRIQUE HERNANDEZ EDGARDO"
        },
        {
            "id": "001-09167",
            "nombre": "PROMOTORA DE DESARROLLOS INMOBILIARIOS, S.A. DE C.V."
        },
        {
            "id": "001-09171",
            "nombre": "SOLDADURA Y MULTISERVICIOS MIRANDA"
        },
        {
            "id": "001-09189",
            "nombre": "MONTANO JOSE FRANCISCO"
        },
        {
            "id": "001-09192",
            "nombre": "CABALLERO ORELLANA  DANILO ALEXANDER"
        },
        {
            "id": "001-09195",
            "nombre": "INVERSIONES CIVILES Y ARQUITECTONICAS DE HONDURAS S DE R L DE C V"
        },
        {
            "id": "001-09199",
            "nombre": "CABALLERO GALINDO IGNACIO"
        },
        {
            "id": "001-09202",
            "nombre": "INMOBILIARIA EL ACTIVO S R L"
        },
        {
            "id": "001-09209",
            "nombre": "REFRINORTE DE HONDURAS S DE RL DE CV"
        },
        {
            "id": "001-09212",
            "nombre": "COMPAÑIA DE SERVICIOS VARIOS S DE R L DE C V"
        },
        {
            "id": "001-09217",
            "nombre": "FUMIGADORA ENAMORADO SRL"
        },
        {
            "id": "001-09219",
            "nombre": "INVERSIONES Y MULTISERVICIOS MONTE BASAN S DE RL DE CV"
        },
        {
            "id": "001-09229",
            "nombre": "DERAS SORTO WILFREDO"
        },
        {
            "id": "001-09239",
            "nombre": "INGENIERIA SERVICIOS Y CONSTUCCION S DE RL"
        },
        {
            "id": "001-09271",
            "nombre": "TABOR MURILLO JORGE EVELIO"
        },
        {
            "id": "001-09277",
            "nombre": "ESPACIO Y ERGONOMIA DE HONDURAS SA"
        },
        {
            "id": "001-09281",
            "nombre": "RUDIS BELTER PINEDA"
        },
        {
            "id": "001-09301",
            "nombre": "ASTI2000"
        },
        {
            "id": "001-09303",
            "nombre": "CONST. PARQUE RECREATIVO COL. MORENO Y HERCULES"
        },
        {
            "id": "001-09305",
            "nombre": "CRUZ ALVAREZ JULIO ALFONSO"
        },
        {
            "id": "001-09317",
            "nombre": "INNOVACIONES METROPOLITANAS SA DE CV"
        },
        {
            "id": "001-09331",
            "nombre": "EMOCON GROUP S A"
        },
        {
            "id": "001-09332",
            "nombre": "CASTELLANOS HIPP JUAN WILFREDO"
        },
        {
            "id": "001-09353",
            "nombre": "JOAMSA SA DE CV"
        },
        {
            "id": "001-09429",
            "nombre": "TURCIOS PEÑA MARCO TALIO"
        },
        {
            "id": "001-09430",
            "nombre": "LEZAMA RODRIGUEZ RONNY ALEXANDER"
        },
        {
            "id": "001-09499",
            "nombre": "SERVICIOS INTEGRADOS DE HONDURAS S A DE C V"
        },
        {
            "id": "001-09503",
            "nombre": "RENTAL SOLUTIONS S DE R L DE CV"
        },
        {
            "id": "001-09507",
            "nombre": "HOME & DECOR LIBELULA S DE RL DE CV"
        },
        {
            "id": "001-09511",
            "nombre": "COMERCIAL VANESSA S DE R L"
        },
        {
            "id": "001-09526",
            "nombre": "CORPORACION Y DESARROLLO S A DE C V"
        },
        {
            "id": "001-09527",
            "nombre": "ALFARO HENRIQUEZ DAISY RAMONA"
        },
        {
            "id": "001-09536",
            "nombre": "CONTRATISTA Y SERVICIOS GENERALES"
        },
        {
            "id": "001-09556",
            "nombre": "CONSORCIO CONSTRUCTORA EL ARENAL"
        },
        {
            "id": "001-09558",
            "nombre": "VILLAS SAN JUAN SA DE CV"
        },
        {
            "id": "001-09565",
            "nombre": "JUNTA DE AGUA BARRIO EL CENTRO CAÑAVERAL"
        },
        {
            "id": "001-09566",
            "nombre": "SOCIEDAD HIDROELECTRICA AGUAS VIVAS S.A. DE C.V."
        },
        {
            "id": "001-09570",
            "nombre": "RIVERA JEREZANO JAIME EDGARDO"
        },
        {
            "id": "001-09581",
            "nombre": "MEJIA PINEDA VICTOR ARTURO"
        },
        {
            "id": "001-09584",
            "nombre": "MELENDEZ CELSO AMADOR"
        },
        {
            "id": "001-09607",
            "nombre": "MINAS Y CUEVAS S.A."
        },
        {
            "id": "001-09617",
            "nombre": "INVERSIONES VALENTINA, S. DE R.L. DE C.V."
        },
        {
            "id": "001-09626",
            "nombre": "AROLDO DERAS MARVIN"
        },
        {
            "id": "001-09629",
            "nombre": "LOGISTICA E INFRAESTRUCTURA ANASSAGORA DE HONDURAS SA DE CV"
        },
        {
            "id": "001-09647",
            "nombre": "INVERSIONES METALICAS TABORA S DE RL"
        },
        {
            "id": "001-09652",
            "nombre": "UNITED TEXTILE OF AMERICA, S. de R.L. de C.V."
        },
        {
            "id": "001-09653",
            "nombre": "LOPEZ BORJAS JENNY SKARLETH"
        },
        {
            "id": "001-09658",
            "nombre": "MARADIAGA DAVIS  EDWIN VICENTE"
        },
        {
            "id": "001-09676",
            "nombre": "CONSTRUCTORA TECH-30 S DE RL"
        },
        {
            "id": "001-09681",
            "nombre": "VALLE MATUTE MARCO TULIO"
        },
        {
            "id": "001-09691",
            "nombre": "SERRANO RAUDA JOSE FERNANDO"
        },
        {
            "id": "001-09694",
            "nombre": "PALENCIA AGUILAR CRISTIAN GERARDO"
        },
        {
            "id": "001-09720",
            "nombre": "DEUTSCH WINDOWS HONDURAS S A"
        },
        {
            "id": "001-09722",
            "nombre": "MARTINEZ CHAVEZ CARLOS EDUARDO"
        },
        {
            "id": "001-09728",
            "nombre": "KAWAS KAWAS RICARDO BISHARA"
        },
        {
            "id": "001-09740",
            "nombre": "ALUMINIOS Y VIDRIOS GUTIERREZ SRL"
        },
        {
            "id": "001-09744",
            "nombre": "PACOL SA DE CV"
        },
        {
            "id": "001-09754",
            "nombre": "GENESIS APPAREL, S. de R. L. de C.V."
        },
        {
            "id": "001-09757",
            "nombre": "GALINDO SOSA MARCO TULIO"
        },
        {
            "id": "001-09763",
            "nombre": "GALDAMEZ RODRIGUEZ JAVIER ANTONIO"
        },
        {
            "id": "001-09779",
            "nombre": "CORPORACION MUNICIPAL DEL MUNICIPIO DE SAN MANUEL"
        },
        {
            "id": "001-09782",
            "nombre": "AUTOCENTRO MOLINARI / MOLINARI HERNANDEZ ARACELI"
        },
        {
            "id": "001-09790",
            "nombre": "BENEFICIO DE CAFE INLOHER S. DE R.L."
        },
        {
            "id": "001-09792",
            "nombre": "VELASQUEZ VELASQUEZ JESUS EDGARDO"
        },
        {
            "id": "001-09803",
            "nombre": "CASTELLANOS SANCHEZ LUIS ALONZO"
        },
        {
            "id": "001-09805",
            "nombre": "INVERSIONES ALUMINIOS Y VIDRIOS ISAAC S DE R L"
        },
        {
            "id": "001-09818",
            "nombre": "QUAN GALDAMEZ EDUARDO"
        },
        {
            "id": "001-09822",
            "nombre": "MORALES MENDOZA MANUEL ALBERTO / AUSEMO"
        },
        {
            "id": "001-09829",
            "nombre": "GARCIA JUAN"
        },
        {
            "id": "001-09830",
            "nombre": "HERNANDEZ PAZ HECTOR RUBEN"
        },
        {
            "id": "001-09831",
            "nombre": "ORGANIZACION PUBLICITARIA SA"
        },
        {
            "id": "001-09835",
            "nombre": "CASTELLANOS MEJIA NORMA YOLANDA / TALLER CASTELLANOS"
        },
        {
            "id": "001-09838",
            "nombre": "RAMOS HERNANDEZ  ENOC DODANIM"
        },
        {
            "id": "001-09848",
            "nombre": "RODRIGUEZ VALLADARES JORGE ALBERTO"
        },
        {
            "id": "001-09877",
            "nombre": "SERVICIOS AUTOMOTRIZ COCHE¿S SRL DE CV"
        },
        {
            "id": "001-09879",
            "nombre": "AGENCIA GLOBAL S.A. DE C.V."
        },
        {
            "id": "001-09897",
            "nombre": "INVERSIONES INMOBILIARIAS FERNANDEZ, SOCIEDAD DE RESPONSABILIDAD LIMITADA DE CAPITAL VARIABLE"
        },
        {
            "id": "001-09901",
            "nombre": "CODER ROBERT H"
        },
        {
            "id": "001-09903",
            "nombre": "JOLLY ROGER CRUISES & TOURS SA"
        },
        {
            "id": "001-09939",
            "nombre": "SOLUCIONES INDUSTRIALES VYM"
        },
        {
            "id": "001-09940",
            "nombre": "TRANSMACAH SRL DE CV"
        },
        {
            "id": "001-09960",
            "nombre": "FUNEZ FERNANDEZ ELISEO"
        },
        {
            "id": "001-09971",
            "nombre": "HONDURAS HOSPITALITY S A DE C V"
        },
        {
            "id": "001-10008",
            "nombre": "CALIFORNIA CUSTOMS S DE R L"
        },
        {
            "id": "001-10030",
            "nombre": "PUERTOS DE CRUCEROS Y MARINA DE LAS ISLAS DE LA BAHIA SA DE CV"
        },
        {
            "id": "001-10048",
            "nombre": "ROSA GUZMAN JOSE ARTURO"
        },
        {
            "id": "001-10055",
            "nombre": "INDUSTRIAS WARE SA"
        },
        {
            "id": "001-10056",
            "nombre": "NEHRING MEJIA JOSE BERNARDO"
        },
        {
            "id": "001-10077",
            "nombre": "JUAN MIGUEL MEJIA PACHECO"
        },
        {
            "id": "001-10081",
            "nombre": "AGROCOMERCIAL GUTIRREZ"
        },
        {
            "id": "001-10084",
            "nombre": "INVERSIONES PASTRANA S DE R L"
        },
        {
            "id": "001-10093",
            "nombre": "SERVICIOS Y CONTRATACIONES RODRIGUEZ S DE R L"
        },
        {
            "id": "001-10095",
            "nombre": "CRUZ ALBERTO ALICIA"
        },
        {
            "id": "001-10112",
            "nombre": "HIDALGO E HIDALGO S.A. SUCURSAL HONDURAS"
        },
        {
            "id": "001-10118",
            "nombre": "AGROINDUSTRIAL GUAYMAS"
        },
        {
            "id": "001-10137",
            "nombre": "VIDRIOS Y PARTES S DE RL DE CV"
        },
        {
            "id": "001-10147",
            "nombre": "CERAMICASA SA DE CV"
        },
        {
            "id": "001-10162",
            "nombre": "MIRALDA REYES JOSE LUIS"
        },
        {
            "id": "001-10163",
            "nombre": "G & S INDUSTRIES SA CV"
        },
        {
            "id": "001-10188",
            "nombre": "JOYA ALVARADO SALVADOR ANTONIO"
        },
        {
            "id": "001-10197",
            "nombre": "EMPRESA DE CONSTRUCCION ALEMAN EUCEDA S DE RL DE CV"
        },
        {
            "id": "001-10205",
            "nombre": "CONSTRUCTORA Y OBRAS DE MARINA HONDURAS, S.A DE C.V"
        },
        {
            "id": "001-10219",
            "nombre": "COPI PRINT SA DE CV"
        },
        {
            "id": "001-10221",
            "nombre": "AUTOSERVICIOS MIGUELITO"
        },
        {
            "id": "001-10223",
            "nombre": "IMPORTACIONES DIGITALES SRL DE CV"
        },
        {
            "id": "001-10231",
            "nombre": "PEREZ TABORA PABLO JOSE"
        },
        {
            "id": "001-10233",
            "nombre": "MARMON SAFETY GROUP SOCIEDAD ANONIMA"
        },
        {
            "id": "001-10251",
            "nombre": "HOTEL CABAÑAS RIVER PARK SA"
        },
        {
            "id": "001-10252",
            "nombre": "CORPORACION DE ENERGIA RENOVABLE SA DE CV"
        },
        {
            "id": "001-10259",
            "nombre": "EUROTEC DE HONDURAS S DE R L"
        },
        {
            "id": "001-10263",
            "nombre": "COMERCIAL CESIA SRL"
        },
        {
            "id": "001-10267",
            "nombre": "MATHEU MEJIA CHARLY OVANDO"
        },
        {
            "id": "001-10279",
            "nombre": "PINTO POSADAS ALEX HUMBERTO"
        },
        {
            "id": "001-10289",
            "nombre": "CALDERON MIRANDA JULIO CESAR"
        },
        {
            "id": "001-10300",
            "nombre": "INMOBILIARIA Y CONSTRUCTORA TOSCANA S DE RL DE CV"
        },
        {
            "id": "001-10306",
            "nombre": "ESCALANTE CAMPOS FRANCISCO ALEXANDER"
        },
        {
            "id": "001-10322",
            "nombre": "INMOBILIARIA VILLA VERDE S DE RL"
        },
        {
            "id": "001-10348",
            "nombre": "PINEDA AYALA EDUARDO BENIGNO"
        },
        {
            "id": "001-10357",
            "nombre": "FIGUEROA GOOD CARLOS GUSTAVO"
        },
        {
            "id": "001-10370",
            "nombre": "CENTRAL AMERICA SPINNING WORKS SA"
        },
        {
            "id": "001-10378",
            "nombre": "TAC AMERICAS,LTD"
        },
        {
            "id": "001-10384",
            "nombre": "CENTRAL DE TUERCAS Y TORNILLOS SA DE CV"
        },
        {
            "id": "001-10415",
            "nombre": "LOPEZ ALVARADO FRANCISCO ARTURO"
        },
        {
            "id": "002-00059",
            "nombre": "SANTOS Y COMPAÑIA S.A. DE C.V."
        },
        {
            "id": "002-00078",
            "nombre": "CONSTRUCTORA SATO, S. DE R.L. DE C.V."
        },
        {
            "id": "002-00109",
            "nombre": "SALVADOR GARCIA Y ASOCIADOS S. de R.L."
        },
        {
            "id": "002-00118",
            "nombre": "INGENIEROS CALONA DE HONDURAS S.DE R.L. DE C.V."
        },
        {
            "id": "002-00123",
            "nombre": "SERVICIO DE INGENIERIA MONTES, S.DE RL. DE C.V."
        },
        {
            "id": "002-00132",
            "nombre": "SERPIC S. de R. L. de C.V"
        },
        {
            "id": "002-00134",
            "nombre": "ASTALDI, S.P.A."
        },
        {
            "id": "002-00144",
            "nombre": "INVERSIONES FR, S. DE R.L. DE C.V."
        },
        {
            "id": "002-00180",
            "nombre": "EMPACADORA SAN LORENZO S.A DE C.V"
        },
        {
            "id": "002-00240",
            "nombre": "RAMCO INGENIEROS CONSTRUCTORES S.de R.L."
        },
        {
            "id": "002-00243",
            "nombre": "CONSTRUCTORA BUCK S.DE R.L. DE C.V."
        },
        {
            "id": "002-00334",
            "nombre": "CONSTRUCCIONES MIDENCE"
        },
        {
            "id": "002-00399",
            "nombre": "TELECOMMUNICATIONS AND NETWORKING S. de R.L. de C.V."
        },
        {
            "id": "002-00623",
            "nombre": "AZUCARERA TRES VALLES"
        },
        {
            "id": "002-00700",
            "nombre": "GUTIERREZ Y ASOCIADOS SRL DE CV"
        },
        {
            "id": "002-00729",
            "nombre": "CONSULTORES EN INGENIERIA  S.A. de C.V."
        },
        {
            "id": "002-00732",
            "nombre": "CONSTRUCTORA GUZMAN S. DE R.L. DE C.V."
        },
        {
            "id": "002-00899",
            "nombre": "INTERAIRPORTS S.A."
        },
        {
            "id": "002-00923",
            "nombre": "VITROALUMINIO, S. de R.L."
        },
        {
            "id": "002-01080",
            "nombre": "DETALLES D' CONSTRUCCION S.DE R.L."
        },
        {
            "id": "002-01473",
            "nombre": "RODIO SWISSBORING HONDURAS, S.A."
        },
        {
            "id": "002-01546",
            "nombre": "CONSTRUCCIONES NABLA HONDURAS S.A. DE C.V."
        },
        {
            "id": "002-01780",
            "nombre": "J F CONSTRUCCIONES S.A. DE C.V."
        },
        {
            "id": "002-01810",
            "nombre": "IMETAL S.DE R.L. DE C.V."
        },
        {
            "id": "002-01858",
            "nombre": "GEOCONSULT S.A DE C.V."
        },
        {
            "id": "002-01870",
            "nombre": "SALCO S.A."
        },
        {
            "id": "002-01965",
            "nombre": "ADMINISTRACION Y SERVICIOS, S.A."
        },
        {
            "id": "002-02072",
            "nombre": "BOMBAS Y MOTORES DE HONDURAS S.A."
        },
        {
            "id": "002-02139",
            "nombre": "CONSTRUDECO, S.A. de C.V."
        },
        {
            "id": "002-02377",
            "nombre": "DESARROLLOS INMOBILIARIOS CASCADAS HONDURAS, S.A. DE C.V."
        },
        {
            "id": "002-02591",
            "nombre": "INDUSTRIAL FERRETERA S.A. de C.V."
        },
        {
            "id": "002-02694",
            "nombre": "CONSTRUCTORA ZERO S. de R.L. de C.V."
        },
        {
            "id": "002-02946",
            "nombre": "POSTENSA S.A."
        },
        {
            "id": "002-02960",
            "nombre": "SECOSUEMP S.de R.L"
        },
        {
            "id": "002-03016",
            "nombre": "APLICANO JOSE"
        },
        {
            "id": "002-03067",
            "nombre": "GRUPO VELA ROATAN S.A."
        },
        {
            "id": "002-03149",
            "nombre": "INMOBILIARIA CONSTRUCCION Y FINANZAS S.A. DE C.V."
        },
        {
            "id": "002-03224",
            "nombre": "SERVICIOS GENERALES SA DE CV"
        },
        {
            "id": "002-03472",
            "nombre": "AVANCE INGENIEROS GRUPO M S. A. DE C.V."
        },
        {
            "id": "002-03596",
            "nombre": "DISEÑO CONSTRUCCION E INVERSIONES S. R. L."
        },
        {
            "id": "002-03743",
            "nombre": "ASOCIACION DE CONSULTORES EN INGENIERIA, S. de R.L. de C.V."
        },
        {
            "id": "002-03773",
            "nombre": "MALUDEC  S. DE R.L. DE C.V."
        },
        {
            "id": "002-03798",
            "nombre": "GALEAS ARQUITECTOS S. DE R.L."
        },
        {
            "id": "002-03833",
            "nombre": "ICADE"
        },
        {
            "id": "002-03859",
            "nombre": "ESCUELA AGRICOLA PANAMERICANA"
        },
        {
            "id": "002-03861",
            "nombre": "INMOBILIARIA PREMIER S.A. DE C.V."
        },
        {
            "id": "002-04024",
            "nombre": "AGROPECUARIA MONTELIBANO"
        },
        {
            "id": "002-04026",
            "nombre": "BAZAR DIANA S. de R.L."
        },
        {
            "id": "002-04034",
            "nombre": "FERRETERIA AL S.DE R.L."
        },
        {
            "id": "002-04035",
            "nombre": "FANASA, S. de R.L. de C.V."
        },
        {
            "id": "002-04042",
            "nombre": "CONSTRUCTORA CASTELLON & ASOCIADOS (Cta. TGU-Lps.)"
        },
        {
            "id": "002-04043",
            "nombre": "CRESCO S. DE R.L."
        },
        {
            "id": "002-04044",
            "nombre": "CONSTRUCTORES ASOCIADOS VARIOS CAVA SA DE  CV"
        },
        {
            "id": "002-04048",
            "nombre": "GRUPO MOLIOR"
        },
        {
            "id": "002-04054",
            "nombre": "CONCREMIX S.A. DE C.V."
        },
        {
            "id": "002-04055",
            "nombre": "A1 ARMOR S.A."
        },
        {
            "id": "002-04057",
            "nombre": "GALERIA LAS LOMAS S.A. de C.V."
        },
        {
            "id": "002-04060",
            "nombre": "AGUA PARA EL PUEBLO"
        },
        {
            "id": "002-04061",
            "nombre": "CONCRETO, EQUIPO Y CONSTRUCCION S. de R.L."
        },
        {
            "id": "002-04069",
            "nombre": "ARGOS HONDURAS S.A. de C.V."
        },
        {
            "id": "002-04074",
            "nombre": "CORPORACION HOTELERA INTERCENTROAMERICANA S.A. DE C.V."
        },
        {
            "id": "002-04076",
            "nombre": "AZUCARERA LA GRECIA S.A. DE C.V."
        },
        {
            "id": "002-04080",
            "nombre": "DESARROLLOS E INVERSIONES WALLACE, S. de R.L."
        },
        {
            "id": "002-04085",
            "nombre": "IGLESIA CATOLICA DE HONDURAS"
        },
        {
            "id": "002-04086",
            "nombre": "PROYECTOS DE CONSTRUCCIÓN Y FOMENTO S. de R.L."
        },
        {
            "id": "002-04090",
            "nombre": "PRODUCTOS DECORATIVOS S.A."
        },
        {
            "id": "002-04094",
            "nombre": "RS INMOBILIARIA S.A. DE C.V."
        },
        {
            "id": "002-04096",
            "nombre": "ECO-LOGICA S.A. DE C.V."
        },
        {
            "id": "002-04097",
            "nombre": "SERVICIOS PROFESIONALES, S.A. de C.V."
        },
        {
            "id": "002-04098",
            "nombre": "CONSTRUCTORA QUEIROZ GALVAO S.A."
        },
        {
            "id": "002-04105",
            "nombre": "HYDROLUZ, S.A. DE C.V."
        },
        {
            "id": "002-04107",
            "nombre": "ALMENDARES PADILLA WILMER RENIERY"
        },
        {
            "id": "002-04108",
            "nombre": "SINOHYDRO CORPORATION LIMITED"
        },
        {
            "id": "002-04120",
            "nombre": "DYNAMIC CORPORATION"
        },
        {
            "id": "002-04122",
            "nombre": "INVERSIONES BARAHONA ZUNIGA"
        },
        {
            "id": "002-04125",
            "nombre": "AJ FITNESS"
        },
        {
            "id": "002-04127",
            "nombre": "LANZA BARAHONA CESAR AUGUSTO"
        },
        {
            "id": "002-04131",
            "nombre": "LASER INVERSIONES S. DE R.L. DE C.V."
        },
        {
            "id": "002-04134",
            "nombre": "A & Z ARQUITECTOS"
        },
        {
            "id": "002-04136",
            "nombre": "IMPULSA"
        },
        {
            "id": "002-04137",
            "nombre": "SUMINISTROS, INGENIERIA, TECNOLOGIA"
        },
        {
            "id": "002-04140",
            "nombre": "VIZION DEVELOPMENTS S.A.DE C.V."
        },
        {
            "id": "002-04144",
            "nombre": "COMPAÑIA DE INFRAESTRUCTURA E INVERSIONES NACIONALES"
        },
        {
            "id": "002-04152",
            "nombre": "INGENIERIA DE CAMINOS S. DE R.L. DE C.V"
        },
        {
            "id": "002-04157",
            "nombre": "SALINAS CHANG FRANCISCO JAVIER"
        },
        {
            "id": "002-04159",
            "nombre": "ALVAREZ Y COMPAÑIA CONSTRUCTORES SA DE CV"
        },
        {
            "id": "002-04167",
            "nombre": "AVANTI INVERSIONES"
        },
        {
            "id": "002-04172",
            "nombre": "STECCO"
        },
        {
            "id": "002-04180",
            "nombre": "CONSTRUCTORA RILO S DE R L"
        },
        {
            "id": "002-04181",
            "nombre": "TALLER SAN JOSE"
        },
        {
            "id": "002-04192",
            "nombre": "TEKTON INGENIERIA"
        },
        {
            "id": "002-04194",
            "nombre": "CONSTRUCTORA CRUZ ARMIJO"
        },
        {
            "id": "002-04201",
            "nombre": "MJ CONSTRUCCIONES Y CONSULTORIA S. DE R.L."
        },
        {
            "id": "002-04212",
            "nombre": "SF CONSTRUCCIONES"
        },
        {
            "id": "002-04215",
            "nombre": "SERVICIOS ESPECIALIZADOS EN MADERAS S. A. DE C. V."
        },
        {
            "id": "002-04218",
            "nombre": "OT INVERSIONES"
        },
        {
            "id": "002-04235",
            "nombre": "PINTER  S DE RL"
        },
        {
            "id": "002-04248",
            "nombre": "MAQUINARIA Y PROYECTOS S.A"
        },
        {
            "id": "002-04250",
            "nombre": "AVILES ROMERO JOSE CIRIACO,"
        },
        {
            "id": "002-04261",
            "nombre": "CONSTRUCTORA VELASQUEZ S DE R L DE C V"
        },
        {
            "id": "002-04271",
            "nombre": "SMART CONTROLS HVAC"
        },
        {
            "id": "002-04273",
            "nombre": "COSCO S DE RL"
        },
        {
            "id": "002-04276",
            "nombre": "SISTEMAS DE ALTA SEGURIDAD, S. de R.L."
        },
        {
            "id": "002-04278",
            "nombre": "NACIONAL DE INGENIEROS ELECTROMECÁNICA S.A. de C.V."
        },
        {
            "id": "002-04281",
            "nombre": "CONSTRUCTORA SAPPHIRUS"
        },
        {
            "id": "002-04290",
            "nombre": "THERMOTEC INGENIERIA Y SERVICIO DE HONDURAS SA DE CV"
        },
        {
            "id": "002-04296",
            "nombre": "FERRETERIA HERCO S DE R L DE C V"
        },
        {
            "id": "002-04311",
            "nombre": "FOLOFO JULIO"
        },
        {
            "id": "002-04313",
            "nombre": "AZUCARERA CHOLUTECA S.A."
        },
        {
            "id": "002-04322",
            "nombre": "CONSTRUCTORA PINEL Y ASOCIADOS S DE RL"
        },
        {
            "id": "002-04330",
            "nombre": "CONSTRUCCIONES, INVERSIONES Y DESAROLLO S.A DE C.V"
        },
        {
            "id": "002-04331",
            "nombre": "ADT ARQUITECTOS S DE RL"
        },
        {
            "id": "002-04336",
            "nombre": "HONDUSERVICE S DE RL"
        },
        {
            "id": "002-04374",
            "nombre": "THYSSENKRUPP ELEVADORES S DE RL"
        },
        {
            "id": "002-04375",
            "nombre": "RESIDENCE SKY CLUB S DE RL"
        },
        {
            "id": "002-04398",
            "nombre": "GEO CONSTRUCCIONES S A DE C V"
        },
        {
            "id": "002-04407",
            "nombre": "HONDURAS MEDICAL CENTER SA DE CV"
        },
        {
            "id": "002-04408",
            "nombre": "BANCO FINANCIERA COMERCIAL HONDUREÑA S.A."
        },
        {
            "id": "002-04412",
            "nombre": "SOCIEDAD DE INGENIEROS CONSTRUCTORES DE HONDURAS S DE R L"
        },
        {
            "id": "002-04413",
            "nombre": "TECKOM"
        },
        {
            "id": "002-04417",
            "nombre": "PROMECASA"
        },
        {
            "id": "002-04495",
            "nombre": "JUAN JOSE MONTOYA/ CASTILLO CACERES"
        },
        {
            "id": "002-04500",
            "nombre": "CLIMAA"
        },
        {
            "id": "002-04524",
            "nombre": "CONSTRUCCIONES ENOC"
        },
        {
            "id": "002-04529",
            "nombre": "RECINOS JORGE"
        },
        {
            "id": "002-04530",
            "nombre": "ATLANTIC ELECTRIC S DE R L DE C V"
        },
        {
            "id": "002-04545",
            "nombre": "EQUINSA ENERGY"
        },
        {
            "id": "002-04583",
            "nombre": "DYPAR S DE RL"
        },
        {
            "id": "002-04587",
            "nombre": "SERMACONSE"
        },
        {
            "id": "002-04596",
            "nombre": "ZAVALA GONZALEZ  CELEO ALBERTO"
        },
        {
            "id": "002-04598",
            "nombre": "FUNDACION DE APOYO AL HOSPITAL ESCUELA"
        },
        {
            "id": "002-04614",
            "nombre": "INMOBILIARIA COSTABECK"
        },
        {
            "id": "002-04635",
            "nombre": "MARMOLES DE HONDURAS S A DE C V"
        },
        {
            "id": "002-04637",
            "nombre": "INVERSIONES COMERCIALES E INDUSTRIALES"
        },
        {
            "id": "002-04648",
            "nombre": "INMOBILIARIA KAFIE S A"
        },
        {
            "id": "002-04655",
            "nombre": "INGENIERIA DEL DISEÑO Y LA CONSTRUCCION S DE RL DE CV"
        },
        {
            "id": "002-04673",
            "nombre": "INGECAP"
        },
        {
            "id": "002-04717",
            "nombre": "SERVICIOS DE CONSTRUCCION O & F S. DE R.L"
        },
        {
            "id": "002-04735",
            "nombre": "GRUPO BIRCASA"
        },
        {
            "id": "002-04800",
            "nombre": "CONSTRUCTORA MADO S DE R L DE C V"
        },
        {
            "id": "002-04833",
            "nombre": "INDUSTRIAS Y SERVICIOS"
        },
        {
            "id": "002-04838",
            "nombre": "MILANO"
        },
        {
            "id": "002-04850",
            "nombre": "DISTRIBUIDORA TEXTIL S.A. DE CV"
        },
        {
            "id": "002-04861",
            "nombre": "SERVICIO DE CONSTRUCCION Y REMODELACION S DE RL"
        },
        {
            "id": "002-04906",
            "nombre": "COINPRO"
        },
        {
            "id": "002-04908",
            "nombre": "CONSTRUCTORA ECO"
        },
        {
            "id": "002-04922",
            "nombre": "DECOARQ"
        },
        {
            "id": "002-04938",
            "nombre": "EDCON"
        },
        {
            "id": "002-04940",
            "nombre": "SUAZO GARCIA RAMON"
        },
        {
            "id": "002-04960",
            "nombre": "EMPRESA PROPIETERIA DE LA RED"
        },
        {
            "id": "002-04961",
            "nombre": "CONSTRUCTIVA S.A DE C.V"
        },
        {
            "id": "002-04963",
            "nombre": "INGENIEROS GARCIA CONSULTORES, S. DE R.L."
        },
        {
            "id": "002-04982",
            "nombre": "MEDINA EDUARDO ALEX"
        },
        {
            "id": "002-05038",
            "nombre": "CONSTRUCTORA ACOSTA"
        },
        {
            "id": "002-05060",
            "nombre": "VIDRIERIA SEGOVIA"
        },
        {
            "id": "002-05089",
            "nombre": "TECNOLOGIA ACCESO Y SEGURIDAD"
        },
        {
            "id": "002-05113",
            "nombre": "FLEFIL Y ASOCIADOS"
        },
        {
            "id": "002-05114",
            "nombre": "INGENIERIA H"
        },
        {
            "id": "002-05133",
            "nombre": "INSTITUTO DE PREVISION MILITAR"
        },
        {
            "id": "002-05134",
            "nombre": "EMBAJADA AMERICANA"
        },
        {
            "id": "002-05159",
            "nombre": "CONSTRUCTORA SIMON"
        },
        {
            "id": "002-05182",
            "nombre": "ALUTECH, S.A. DE C.V."
        },
        {
            "id": "002-05196",
            "nombre": "MULTISERVICIOS VARGAS"
        },
        {
            "id": "002-05225",
            "nombre": "INDUSTRIA TECNICA METALICA SA"
        },
        {
            "id": "002-05249",
            "nombre": "CONASA"
        },
        {
            "id": "002-05284",
            "nombre": "MARTINEZ ROBERTO"
        },
        {
            "id": "002-05287",
            "nombre": "HONDURAS CONSTRUCTORES"
        },
        {
            "id": "002-05314",
            "nombre": "ING PARA EL DESARROLLO SRL CV"
        },
        {
            "id": "002-05341",
            "nombre": "GRUPO Q HONDURAS"
        },
        {
            "id": "002-05373",
            "nombre": "PAVIMENTOS Y CAMINOS"
        },
        {
            "id": "002-05377",
            "nombre": "ZEPEDA DAVID"
        },
        {
            "id": "002-05379",
            "nombre": "CONSTRUCTORA SUALEMAN"
        },
        {
            "id": "002-05398",
            "nombre": "CONSTRUCTORA LEMPIRA"
        },
        {
            "id": "002-05418",
            "nombre": "OBANDO CONSTRUCCIONES"
        },
        {
            "id": "002-05435",
            "nombre": "CORDOVA EDITH"
        },
        {
            "id": "002-05440",
            "nombre": "ALVARADO GUSTAVO ADOLFO"
        },
        {
            "id": "002-05510",
            "nombre": "CONSORCIO SALVADOR GARCIA SERPIC S.A"
        },
        {
            "id": "002-05524",
            "nombre": "MARADIAGA ATAHUALPA"
        },
        {
            "id": "002-05527",
            "nombre": "ALUMINIO VIDRIO Y COMERCIO  SE DE RL DE CV"
        },
        {
            "id": "002-05528",
            "nombre": "RIOS JIMENES ALLAN ONAN"
        },
        {
            "id": "002-05535",
            "nombre": "CENTRO TOPOGRAFICO E INGENIERIA S DE R.L"
        },
        {
            "id": "002-05544",
            "nombre": "OQUELI LICONA LEONEL"
        },
        {
            "id": "002-05558",
            "nombre": "ANDINO RAUDALES ALEX ALBERTO"
        },
        {
            "id": "002-05562",
            "nombre": "GARCIA MATUTE EDDUIN ENOC"
        },
        {
            "id": "002-05575",
            "nombre": "VITECH S. DE R.L"
        },
        {
            "id": "002-05594",
            "nombre": "CONSTRUCTORA CELAQUE"
        },
        {
            "id": "002-05608",
            "nombre": "FUMESOH"
        },
        {
            "id": "002-05622",
            "nombre": "BRICEÑO Y ASOCIADOS S DE R L"
        },
        {
            "id": "002-05624",
            "nombre": "PROYECTOS MECANIZADOS, S.A. de C.V."
        },
        {
            "id": "002-05635",
            "nombre": "LINEAS ARQUITECTOS"
        },
        {
            "id": "002-05657",
            "nombre": "COMP CONSTRUC SERV MULTIPLESSA CV"
        },
        {
            "id": "002-05668",
            "nombre": "TECNICA DE INGENIERIA S.A"
        },
        {
            "id": "002-05693",
            "nombre": "VARGAS CARIAS  MARIO RODOLFO"
        },
        {
            "id": "002-05701",
            "nombre": "CONSORCIO JF CONSTRUCCIONES-SANTOS Y COMPAÑIA"
        },
        {
            "id": "002-05710",
            "nombre": "ELEVACIONES TECNICAS S A DE C V"
        },
        {
            "id": "002-05719",
            "nombre": "CONSTRUCTORA OSSA LOPEZ S.A.S."
        },
        {
            "id": "002-05744",
            "nombre": "INGENIEROS CONSTRUCTORES Y CONSULTORES S DE RL"
        },
        {
            "id": "002-05758",
            "nombre": "INDUSTRIAS DE PLASTICOS S A DE C V"
        },
        {
            "id": "002-05767",
            "nombre": "INVERSIONES MORAZAN SA"
        },
        {
            "id": "002-05768",
            "nombre": "POLLO INTERNACIONAL S.A. DE C.V."
        },
        {
            "id": "002-05786",
            "nombre": "AVILA PERDOMO FRANCISCO VENANCIO"
        },
        {
            "id": "002-05795",
            "nombre": "CUADRA BURLERO VICTOR"
        },
        {
            "id": "002-05807",
            "nombre": "CONASH, S DE R.L DE C.V"
        },
        {
            "id": "002-05845",
            "nombre": "PATRONATO PRO MEJORAMIENTO DE LOS CONDOMINIOS LAS MARIAS"
        },
        {
            "id": "002-05846",
            "nombre": "RICO CASTRO CARLOS JOSE"
        },
        {
            "id": "002-05873",
            "nombre": "HERNANDEZ GONZALES PABLO SAID"
        },
        {
            "id": "002-05911",
            "nombre": "INSTITUTO DE LA PROPIEDAD (IP)"
        },
        {
            "id": "002-05921",
            "nombre": "VILLATORO AGUILAR ALEJANDRO"
        },
        {
            "id": "002-05922",
            "nombre": "SALGADO GARCIA ROBERTO"
        },
        {
            "id": "002-05954",
            "nombre": "DESARROLLO CONSTRUCCIONES COPAN S L"
        },
        {
            "id": "002-05990",
            "nombre": "ESPINOZA ZERON JOSE FRANCISCO"
        },
        {
            "id": "002-06006",
            "nombre": "SECRETARIA DE RELACIONES EXTERIORES"
        },
        {
            "id": "002-06013",
            "nombre": "MORALES ZELAYA WILMAN DANILO"
        },
        {
            "id": "002-06060",
            "nombre": "IGLESIA DE JESUCRISTO DE LOS SANTOS DE LOS ULTIMOS DIAS"
        },
        {
            "id": "002-06067",
            "nombre": "CONSTRUCTORA MYN S DE R L DE C V"
        },
        {
            "id": "002-06079",
            "nombre": "PRISMA, S. DE R.L. C.V."
        },
        {
            "id": "002-06080",
            "nombre": "KUAN ESPINOZA JAVIER, GERMAN"
        },
        {
            "id": "002-06097",
            "nombre": "INVERSIONES DE LEPAGUARE S. DE R. L. DE C. V."
        },
        {
            "id": "002-06101",
            "nombre": "LOPEZ DUBON FRANCISCO CRUZ"
        },
        {
            "id": "002-06104",
            "nombre": "ORDOÑEZ CABRERA MARVIN RIGOBERTO"
        },
        {
            "id": "002-06115",
            "nombre": "MARTINEZ RODRIGUEZ ALEXIS"
        },
        {
            "id": "002-06134",
            "nombre": "FRANCISCO NOEL, GARCIA ARMIJO"
        },
        {
            "id": "002-06156",
            "nombre": "CACERES NUÑEZ GABRIEL ANDRES"
        },
        {
            "id": "002-06179",
            "nombre": "CONSTRUCTORA MIRALDA VALLECILLO( C O M I V A S A)"
        },
        {
            "id": "002-06196",
            "nombre": "INDUSTRIA Y CONSTRUCCION HONDURAS S. DE R.L"
        },
        {
            "id": "002-06197",
            "nombre": "PONCE MARTINEZ RAMON RAFAEL"
        },
        {
            "id": "002-06207",
            "nombre": "AMADOR SANCHEZ DARWIN OMAR/TRELEC"
        },
        {
            "id": "002-06246",
            "nombre": "CONSTRUCTORA INVERSIONES Y REPRESENTACIONES CASCO Y ASOCIADOS S DE RL"
        },
        {
            "id": "002-06247",
            "nombre": "RIVERA ALONZO GLORIA  MARIA"
        },
        {
            "id": "002-06261",
            "nombre": "COINFA S DE R L DE C V"
        },
        {
            "id": "002-06288",
            "nombre": "FAJARDO OSMAN RICARDO"
        },
        {
            "id": "002-06370",
            "nombre": "INVERSIONES CELAQUE SA"
        },
        {
            "id": "002-06377",
            "nombre": "INCONSUL S DE R L"
        },
        {
            "id": "002-06387",
            "nombre": "ZELAYA RODRIGUEZ VICTORIA ELVIA"
        },
        {
            "id": "002-06404",
            "nombre": "GERMAR FERRETERIA S DE R L DE C V"
        },
        {
            "id": "002-06407",
            "nombre": "CONSTRUCTORA REHOBOT S A DE C V"
        },
        {
            "id": "002-06429",
            "nombre": "LOPEZ CARBAJAL LENIN AQUILES"
        },
        {
            "id": "002-06462",
            "nombre": "INGENIERIA PROGRAMADA SA DE CV"
        },
        {
            "id": "002-06466",
            "nombre": "SERVICIOS TECNICOS ESPECIALIZADOS S DE RL DE CV ,"
        },
        {
            "id": "002-06483",
            "nombre": "CRUZ BERBINSKI JAVIER ALBERTO"
        },
        {
            "id": "002-06519",
            "nombre": "GRANJAS MARINAS SAN BERNARDO S A C V"
        },
        {
            "id": "002-06529",
            "nombre": "CONSTRUCTORA C C SOCIEDAD ANONIMA DE CAPITAL VARIABLE"
        },
        {
            "id": "002-06543",
            "nombre": "EQUILIBRIUM DEVELOPMENT S DE R L"
        },
        {
            "id": "002-06551",
            "nombre": "GUTIERREZ PORTILLO OSCAR ROLANDO"
        },
        {
            "id": "002-06586",
            "nombre": "AMADOR LOPEZ JOSE DAVID"
        },
        {
            "id": "002-06587",
            "nombre": "RODRIGUEZ HECTOR"
        },
        {
            "id": "002-06590",
            "nombre": "GRUPO GYT S A DE C V"
        },
        {
            "id": "002-06600",
            "nombre": "WM CONSTRUCTORES CCG S A DE C V"
        },
        {
            "id": "002-06613",
            "nombre": "RAIZ CAPITAL S A DE C V"
        },
        {
            "id": "002-06631",
            "nombre": "FLORES MOYA DAVID ABRAHAM"
        },
        {
            "id": "002-06654",
            "nombre": "ZELAYA, LUIS CARLOS"
        },
        {
            "id": "002-06657",
            "nombre": "HR CONSTRUCCIONES S DE RL DE CV"
        },
        {
            "id": "002-06701",
            "nombre": "INSTALACION Y SUMINISTROS ELECTRICOS OSORIO AMADOR ISE-O/A S DE R L DE C V"
        },
        {
            "id": "002-06706",
            "nombre": "GRUPO INVEMECO S DE RL DE CV"
        },
        {
            "id": "002-06732",
            "nombre": "PROYECTOS INNOVADORES S A DE C V"
        },
        {
            "id": "002-06761",
            "nombre": "CANALES AREVALO MARIZA YURYANY"
        },
        {
            "id": "002-06784",
            "nombre": "AVICOLA GANADERA SAN ANTONIO S DE R L DE C V"
        },
        {
            "id": "002-06790",
            "nombre": "COPHSA"
        },
        {
            "id": "002-06791",
            "nombre": "CONSTRUCTORA INNOVA S DE RL"
        },
        {
            "id": "002-06801",
            "nombre": "GODOY ZEPEDA WILFREDO"
        },
        {
            "id": "002-06811",
            "nombre": "AGROINDUSTRIAS DIADEMA ZONA FRANCA HONDURAS S A"
        },
        {
            "id": "002-06812",
            "nombre": "VEGA MOLINA MARIO ANTONIO"
        },
        {
            "id": "002-06837",
            "nombre": "HOTELES DE HONDURAS S A DE C V"
        },
        {
            "id": "002-06853",
            "nombre": "VENTURA DIAZ NELSON FRANSUA"
        },
        {
            "id": "002-06886",
            "nombre": "CONSTRUCCIONES Y ALQUILERES M&A SA DE CV"
        },
        {
            "id": "002-06894",
            "nombre": "DELCAMPO INTERNATIONAL SCHOOL S A DE C V"
        },
        {
            "id": "002-06895",
            "nombre": "DISEÑOS Y CONSTRUCCIONES ELECTROMECANICAS S. DE R. L. DE C.V."
        },
        {
            "id": "002-06921",
            "nombre": "BETANCOURT GARCIA GABRIELA SUYAPA"
        },
        {
            "id": "002-06977",
            "nombre": "LOVO ORELLANA JOSE MARTIN"
        },
        {
            "id": "002-06990",
            "nombre": "CENTRO DE ESTUDIO Y ACCION PARA EL DESARROLLO DE HOND. CESADE-HOND."
        },
        {
            "id": "002-06999",
            "nombre": "ALEMAN MANZANAREZ NILSON SAUD"
        },
        {
            "id": "002-07033",
            "nombre": "INNOVATIVE BUSINESS SOLUTIONS SA DE CV"
        },
        {
            "id": "002-07044",
            "nombre": "MEJIA MEJIA MERCEDES ROSAURA"
        },
        {
            "id": "002-07054",
            "nombre": "SERVICIOS MULTIPLES GLOBALES S. DE R.L."
        },
        {
            "id": "002-07056",
            "nombre": "SERVICE AND TRADING BUSINESS SA DE CV ,"
        },
        {
            "id": "002-07086",
            "nombre": "RAMOS ALEMAN JOSE MIGUEL"
        },
        {
            "id": "002-07096",
            "nombre": "ALEMAN COLINDRES JOSE ALEJANDRO"
        },
        {
            "id": "002-07125",
            "nombre": "INVERSIONES MAFER S A"
        },
        {
            "id": "002-07135",
            "nombre": "SANTOS MEJIA CESAR ALFONSO/RYS INVERSIONES"
        },
        {
            "id": "002-07158",
            "nombre": "GRUPO MEY-KO S.A"
        },
        {
            "id": "002-07179",
            "nombre": "PARADA FONSECA JOSUE ISAIAS/DYTAL"
        },
        {
            "id": "002-07186",
            "nombre": "MARADIAGA GUNERA NELSON YOVANY"
        },
        {
            "id": "002-07195",
            "nombre": "CRUZ SANCHEZ MIGUEL ANGEL"
        },
        {
            "id": "002-07198",
            "nombre": "BANEGAS RODRIGUEZ MARCO ANTONIO"
        },
        {
            "id": "002-07212",
            "nombre": "HCC COMPANY, S. DE R. L. DE C. V."
        },
        {
            "id": "002-07259",
            "nombre": "SEIJIRO YAZAWA IWAI HONDURAS S.A."
        },
        {
            "id": "002-07269",
            "nombre": "UMANZOR CONTRERAS AMILCAR"
        },
        {
            "id": "002-07279",
            "nombre": "MEJIA HERNANDEZ ELIAS JOEL"
        },
        {
            "id": "002-07282",
            "nombre": "ELECTROCON NETWORKS"
        },
        {
            "id": "002-07311",
            "nombre": "GODOY BONILLA KRISTIAN JAIR"
        },
        {
            "id": "002-07332",
            "nombre": "MEJIA MARTINEZ EDUYN JOSE"
        },
        {
            "id": "002-07339",
            "nombre": "SANCHEZ NUÑEZ FAUSTO MAURICIO"
        },
        {
            "id": "002-07343",
            "nombre": "INVERSIONES M3F S DE R L"
        },
        {
            "id": "002-07369",
            "nombre": "FLORES ZAPATA LIDA AMANDA"
        },
        {
            "id": "002-07418",
            "nombre": "ALVAREZ HERRERA MARIO ROBERTO"
        },
        {
            "id": "002-07460",
            "nombre": "DIAGNOSTICOS MEDICOS S A"
        },
        {
            "id": "002-07462",
            "nombre": "MOTIÑO FIGUEROA GEMRY ISAUL"
        },
        {
            "id": "002-07495",
            "nombre": "FABRICA DE MUEBLES EL EBANISTA S DE R L DE C V"
        },
        {
            "id": "002-07510",
            "nombre": "TORRES Y ASOCIADOS CONSTRUCTORES S DE R L"
        },
        {
            "id": "002-07544",
            "nombre": "PORTILLO ALFARO JOSE ANGEL"
        },
        {
            "id": "002-07559",
            "nombre": "CORPORACION SOL S. A. DE C. V."
        },
        {
            "id": "002-07563",
            "nombre": "MACCEL S DE R L DE C V"
        },
        {
            "id": "002-07580",
            "nombre": "ORTIZ COREA CONSTANTINO"
        },
        {
            "id": "002-07585",
            "nombre": "OSORTO LOBO YESIBEL"
        },
        {
            "id": "002-07609",
            "nombre": "ARIDOS DE HONDURAS S A"
        },
        {
            "id": "002-07639",
            "nombre": "CONSTRUCTORA PAYES S DE R L"
        },
        {
            "id": "002-07665",
            "nombre": "INGENIEROS CONSULTORES Y CONSTRUCTORES ELECTROMECANICOS PROYECTOS ESPECIALES S A DE C V"
        },
        {
            "id": "002-07676",
            "nombre": "MARTINEZ SILVA ALLAN ALEXIS"
        },
        {
            "id": "002-07684",
            "nombre": "INSTITUTO UNION ESFUERZO Y DEMOCRACIA"
        },
        {
            "id": "002-07685",
            "nombre": "SOCIEDAD BIBLICA DE HONDURAS"
        },
        {
            "id": "002-07705",
            "nombre": "CONSTRUCTORA DINA'C S DE R L DE C V"
        },
        {
            "id": "002-07724",
            "nombre": "CRUZ CALIX ANA DOLORES"
        },
        {
            "id": "002-07749",
            "nombre": "CONSTRUCTORA EL CORRALITO S DE R L"
        },
        {
            "id": "002-07766",
            "nombre": "ALONSO FUNEZ  ROY ALEJANDRO"
        },
        {
            "id": "002-07773",
            "nombre": "AUDIO VISUAL TECHNOLOGY S R L"
        },
        {
            "id": "002-07780",
            "nombre": "ASOCIACION IGLESIA EVANGELICA LINAJE DE ABRAHAM"
        },
        {
            "id": "002-07781",
            "nombre": "HONDURAS BIOMASS ENERGY S A DE C V"
        },
        {
            "id": "002-07788",
            "nombre": "ASOCIACION HOND PARA LA PROMOCION EDUCATIVA AHPE"
        },
        {
            "id": "002-07789",
            "nombre": "MARADIAGA SANTOS ANIBAL"
        },
        {
            "id": "002-07799",
            "nombre": "CASTILLO SALGADO NORMA MARCELA"
        },
        {
            "id": "002-07802",
            "nombre": "ROCK MUSALLAM JISELE PATRICIA"
        },
        {
            "id": "002-07803",
            "nombre": "CORPORACION HORIZONTE SA DE CV"
        },
        {
            "id": "002-07814",
            "nombre": "NELSON CERRATO GEOVANNY RAFAEL"
        },
        {
            "id": "002-07815",
            "nombre": "LOPEZ FLORES JOSE ELOY"
        },
        {
            "id": "002-07818",
            "nombre": "SITRAMEDHYS"
        },
        {
            "id": "002-07819",
            "nombre": "LAGOS IZAGUIRRE EDWIN EDUARDO"
        },
        {
            "id": "002-07821",
            "nombre": "RODRIGUEZ NUÑEZ HAROLD ARIEL"
        },
        {
            "id": "002-07825",
            "nombre": "SOLUCIONES GEOTECNICAS S DE RL"
        },
        {
            "id": "002-07835",
            "nombre": "ALTIA TECHNOLOGY PARK S A DE C V"
        },
        {
            "id": "002-07839",
            "nombre": "CRUZ GARCIA FRANCISCO ORLANDO"
        },
        {
            "id": "002-07842",
            "nombre": "MEDINA CRUZ ROY EMILIO"
        },
        {
            "id": "002-07844",
            "nombre": "ESCOBER OLIVA SERGIO IVAN"
        },
        {
            "id": "002-07847",
            "nombre": "RAMOS VALLADARES KELVIN ARMANDO"
        },
        {
            "id": "002-07857",
            "nombre": "LANDAVERDE MEJIA LIDIA MARIA"
        },
        {
            "id": "002-07861",
            "nombre": "VIENTOS DE ELECTROTECNIA S A DE C V"
        },
        {
            "id": "002-07862",
            "nombre": "ALQUILER DE CARROS SA DE CV"
        },
        {
            "id": "002-07864",
            "nombre": "TEGUCIGALPA TENNIS CLUB SA"
        },
        {
            "id": "002-07865",
            "nombre": "BARAHONA TORRES MARCO TULIO"
        },
        {
            "id": "002-07867",
            "nombre": "PAZ PERDOMO OSCAR LUIS"
        },
        {
            "id": "002-07873",
            "nombre": "CANALES FLORES SANTOS MAXIMINO"
        },
        {
            "id": "002-07907",
            "nombre": "YANES ORTIZ RICARDO ALBERTO"
        },
        {
            "id": "002-07910",
            "nombre": "POSSE BERTOT LETICIA"
        },
        {
            "id": "002-07918",
            "nombre": "SANCHEZ ALMENDAREZ ESLIN RENE"
        },
        {
            "id": "002-07920",
            "nombre": "COMERCIAL FRANCO"
        },
        {
            "id": "002-07929",
            "nombre": "MEJIA VARGAS JOSE ANDRES"
        },
        {
            "id": "002-07935",
            "nombre": "INGENIERIA Y CONSULTORIA EN SEGURIDAD .S.A .DE .C.V"
        },
        {
            "id": "002-07946",
            "nombre": "OCHOA POSSE GABRIELA ALEJANDRA"
        },
        {
            "id": "002-07948",
            "nombre": "INMOBILIARIA RH S DE RL DE CV"
        },
        {
            "id": "002-07959",
            "nombre": "DE LA O ALONZO RIVERA CAROLINA"
        },
        {
            "id": "002-07963",
            "nombre": "RODRIGUEZ GALO FRANKLIN JOSE"
        },
        {
            "id": "002-07967",
            "nombre": "ALVAREZ MENA ALANA MARBELLA"
        },
        {
            "id": "002-07969",
            "nombre": "CINSA FOA FIS"
        },
        {
            "id": "002-07973",
            "nombre": "AGUILAR ALVARENGA JUAN CARLOS"
        },
        {
            "id": "002-07974",
            "nombre": "ELVIR FLORES NELSON FERNANDO"
        },
        {
            "id": "002-07983",
            "nombre": "ASOCIACION DE DESARROLLO COMUNITARIO (ASODECO)"
        },
        {
            "id": "002-07992",
            "nombre": "SOSA AMADOR CARLOS ALBERTO"
        },
        {
            "id": "002-08009",
            "nombre": "RAMIREZ GARCIA OSCAR NOE"
        },
        {
            "id": "002-08010",
            "nombre": "SORIANO SALGADO OSCAR ALBERTO"
        },
        {
            "id": "002-08025",
            "nombre": "LAGOS FLORES GLENDA XIOMARA"
        },
        {
            "id": "002-08028",
            "nombre": "SALINAS AYALA WILSON RIGOBERTO"
        },
        {
            "id": "002-08032",
            "nombre": "ELVIR BALDOVINOS HECTOR MAURICIO"
        },
        {
            "id": "002-08033",
            "nombre": "MATAMOROS GARAY FRANCISCO ANTONIO"
        },
        {
            "id": "002-08037",
            "nombre": "ALICE S GOLDSTEIN"
        },
        {
            "id": "002-08045",
            "nombre": "MAYORGA ARGUETA MARCO TULIO"
        },
        {
            "id": "002-08058",
            "nombre": "ELBERT NIEVES"
        },
        {
            "id": "002-08059",
            "nombre": "CARCAMO MEJIA CARLOS ARTURO"
        },
        {
            "id": "002-08061",
            "nombre": "FLORES FLORES JOSE HERNESTO"
        },
        {
            "id": "002-08062",
            "nombre": "SIMON SALAZAR NAZRY NEGUIB"
        },
        {
            "id": "002-08074",
            "nombre": "FUNES HERRERA ELSY DALILA"
        },
        {
            "id": "002-08076",
            "nombre": "ASOCIACION AGROINDUSTRIAL DE PALMICULTORES DE SABA S A DE C V"
        },
        {
            "id": "002-08079",
            "nombre": "LUCY MICHELLE INGENIERIA S. DE R. L."
        },
        {
            "id": "002-08081",
            "nombre": "IGLESIA DE DIOS EN HONDURAS DE COL. EL SITIO"
        },
        {
            "id": "002-08102",
            "nombre": "D'VICENTE MELGAR SABY CAROLINA"
        },
        {
            "id": "002-08108",
            "nombre": "ANDINO LAGOS MARLON"
        },
        {
            "id": "002-08120",
            "nombre": "SERVI GLASS S DE R L"
        },
        {
            "id": "002-08134",
            "nombre": "INMOBILIARIA PATRIMONIO Y ARRIENDO S.A. (INPASA)"
        },
        {
            "id": "002-08138",
            "nombre": "SUPLIDORA UNIVERSAL EXPRESS S DE R L DE C V"
        },
        {
            "id": "002-08151",
            "nombre": "SIERRA CANALES JORGE ALEJANDRO"
        },
        {
            "id": "002-08161",
            "nombre": "FUENTES LANZA EDUARDO ENRIQUE"
        },
        {
            "id": "002-08164",
            "nombre": "INGENIERIA LOGISTICA HONDURAS, S.A. de C.V."
        },
        {
            "id": "002-08167",
            "nombre": "MAYR ALCANTARA ROGER"
        },
        {
            "id": "002-08168",
            "nombre": "GARCIA CASCO MARTHA ALICIA"
        },
        {
            "id": "002-08171",
            "nombre": "SCOPO S A DE C V"
        },
        {
            "id": "002-08172",
            "nombre": "ZAMORA ESCOTO RAFAEL AUGUSTO"
        },
        {
            "id": "002-08184",
            "nombre": "INVERSIONES DIVERSAS A&M S DE R L"
        },
        {
            "id": "002-08185",
            "nombre": "BARAHONA REYES ERNESTO"
        },
        {
            "id": "002-08186",
            "nombre": "SEGOVIA CARBAJAL MARCIA ALEJANDRA"
        },
        {
            "id": "002-08191",
            "nombre": "SAUCEDA HERNANDEZ ORFILIA ESTELA"
        },
        {
            "id": "002-08201",
            "nombre": "RODAS FLORES JUAN BAUTISTA"
        },
        {
            "id": "002-08204",
            "nombre": "TRANSPORTES RIVERA S DE RL"
        },
        {
            "id": "002-08210",
            "nombre": "VENTA DE REPUESTOS Y MATERIALES DE CONSTRUCCION S DE R L"
        },
        {
            "id": "002-08224",
            "nombre": "CHANDIAS ZUNIGA  MARBIN ALFREDO"
        },
        {
            "id": "002-08230",
            "nombre": "URQUIA FIGUEROA JORGE ALBERTO"
        },
        {
            "id": "002-08242",
            "nombre": "VENTURA ENRIQUEZ NORMAN ELI"
        },
        {
            "id": "002-08257",
            "nombre": "BACA FLORES CRESCENSIO ISAY"
        },
        {
            "id": "002-08260",
            "nombre": "EXPOR VENT HONDURAS, S.A. DE C.V."
        },
        {
            "id": "002-08270",
            "nombre": "SERVICIOS TECNICOS LINESYS"
        },
        {
            "id": "002-08271",
            "nombre": "HERNANDEZ IMBODEN DEBBIE"
        },
        {
            "id": "002-08272",
            "nombre": "VILCHEZ PEREZ WALTHER DAVID"
        },
        {
            "id": "002-08288",
            "nombre": "SCHACHER KAFATI RODERICK ALEXANDER"
        },
        {
            "id": "002-08291",
            "nombre": "BARAHONA JIMENEZ  NILTON EDUARDO"
        },
        {
            "id": "002-08298",
            "nombre": "SCHACHER KAFATI JONATHAN SAMUEL"
        },
        {
            "id": "002-08300",
            "nombre": "TECNICA EUROPEA S A DE C V"
        },
        {
            "id": "002-08301",
            "nombre": "C M AIRLINES S DE R L"
        },
        {
            "id": "002-08309",
            "nombre": "COROJO TABACO SA"
        },
        {
            "id": "002-08310",
            "nombre": "AGUILAR PONCE  LUIS HERNAN"
        },
        {
            "id": "002-08323",
            "nombre": "BENAVIDES SALINAS BLANCA MARIA"
        },
        {
            "id": "002-08327",
            "nombre": "CARTAGENA ARITA REINA JOSELINE"
        },
        {
            "id": "002-08331",
            "nombre": "BONILLA VALLADARES JUAN CARLOS"
        },
        {
            "id": "002-08335",
            "nombre": "INMOBILIARIA ISULA S A"
        },
        {
            "id": "002-08337",
            "nombre": "INCLAM S A"
        },
        {
            "id": "002-08338",
            "nombre": "INDUSTRIA CAMARONERA DEL SUR S A C V"
        },
        {
            "id": "002-08341",
            "nombre": "EUCEDA AMILCAR FRANCO"
        },
        {
            "id": "002-08343",
            "nombre": "SECURITY GUARD SERVICES SA"
        },
        {
            "id": "002-08346",
            "nombre": "KAMALDY S DE R L DE C V"
        },
        {
            "id": "002-08353",
            "nombre": "HN SERVICIOS SA"
        },
        {
            "id": "002-08354",
            "nombre": "ELEVEN BARBER SHOP, S. DE R. L"
        },
        {
            "id": "002-08371",
            "nombre": "PROVEEDORA DE MATERIALES S DE R L DE C V"
        },
        {
            "id": "002-08395",
            "nombre": "INFERCON S DE RL DE C V"
        },
        {
            "id": "002-08413",
            "nombre": "DISTRIBUCIONES JAP S DE RL"
        },
        {
            "id": "002-08415",
            "nombre": "GUEVARA ORDOÑEZ ALEX ROBERTO"
        },
        {
            "id": "002-08416",
            "nombre": "CUBAS LOPEZ NERY DANIEL"
        },
        {
            "id": "002-08419",
            "nombre": "BERLIOZ BUSTILLO ESTEBANA"
        },
        {
            "id": "002-08422",
            "nombre": "VIDEA IRIAS ELVIN OMAR"
        },
        {
            "id": "002-08426",
            "nombre": "RC INGENIEROS CONSULTORES S. DE R.L"
        },
        {
            "id": "002-08431",
            "nombre": "CARDONA AGUILAR LESBIA FANNY"
        },
        {
            "id": "002-08439",
            "nombre": "OLIVA NUÑEZ MARLON GEOVANNY"
        },
        {
            "id": "002-08440",
            "nombre": "PINEDA CABRERA ROBERTO"
        },
        {
            "id": "002-08445",
            "nombre": "DIAMOND ELECTRIC COMPANY, SOCIEDAD DE RESPONSABILIDAD LIMITADA"
        },
        {
            "id": "002-08447",
            "nombre": "ZACARIAS SANCHEZ JAIME"
        },
        {
            "id": "002-08448",
            "nombre": "WINDOW SOLUCIONES, S. DE R.L"
        },
        {
            "id": "002-08471",
            "nombre": "ADMINISTRACION, ARQUITECTURA, DIRECCION Y CONSTRUCCION COMPAÑIA Y GESTION S. DE R.L."
        },
        {
            "id": "002-08477",
            "nombre": "CARRASCO MONCADA  JOSUE ALEJANDRO"
        },
        {
            "id": "002-08487",
            "nombre": "BLANCANAPAZ DESIGN SOCIEDAD DE RESPONSABILIDAD LIMITADA"
        },
        {
            "id": "002-08491",
            "nombre": "POSTENSA CENTRO CIVICO SOCIEDAD DE PROPOSITO EXCLUSIVO, S.A"
        },
        {
            "id": "002-08492",
            "nombre": "ENGINEERS & SOLUTIONS S. DE R. L"
        },
        {
            "id": "002-08498",
            "nombre": "MDS ESTRUCTURA METAL HONDURAS CCG, S.A. DE C.V."
        },
        {
            "id": "002-08501",
            "nombre": "FUNEZ MONCADA FERNANDO DAVID"
        },
        {
            "id": "002-08509",
            "nombre": "INVERSIONES INDUSTRIALES S A DE C V"
        },
        {
            "id": "002-08525",
            "nombre": "INVERSIONES DIVERSA DE ORIENTE S. DE R.L."
        },
        {
            "id": "002-08529",
            "nombre": "SANTOS INDUSTRIA S A"
        },
        {
            "id": "002-08533",
            "nombre": "JAVA INVESTMENTS S A DE C V"
        },
        {
            "id": "002-08544",
            "nombre": "GOMEZ PINTO JORGE SAUL"
        },
        {
            "id": "002-08570",
            "nombre": "AGUILAR CARDONA CESAR FRANCISCO"
        },
        {
            "id": "002-08594",
            "nombre": "PONCE RODRIGUEZ JESUS AUGUSTO."
        },
        {
            "id": "002-08611",
            "nombre": "CASA FUTURA S. DE R.L. DE C.V"
        },
        {
            "id": "002-08619",
            "nombre": "DECO CCG SOCIEDAD DE RESPONSABILIDAD LIMITADA"
        },
        {
            "id": "002-08630",
            "nombre": "HERNANDEZ ACOSTA JUAN RAMON"
        },
        {
            "id": "002-08637",
            "nombre": "SARMIENTO RUBI TOMAS ROBERTO"
        },
        {
            "id": "002-08641",
            "nombre": "INGAZE S DE RL DE CV"
        },
        {
            "id": "002-08642",
            "nombre": "GENERAL DE REPUESTOS S A DE C V"
        },
        {
            "id": "002-08648",
            "nombre": "SOSA CRUZ CESAR DAVID"
        },
        {
            "id": "002-08658",
            "nombre": "CONSTRUCTORA RIVERA AGUILAR GREEN PROJECTS S DE R L DE C V"
        },
        {
            "id": "002-08661",
            "nombre": "ESTUDIO DE ARQUITECTURA Y DISEÑO S DE R L"
        },
        {
            "id": "002-08676",
            "nombre": "GRUPO FERRETERO S. DE R.L."
        },
        {
            "id": "002-08688",
            "nombre": "MARTINEZ MEJIA EMILIN ZENOBIA"
        },
        {
            "id": "002-08690",
            "nombre": "LEONEL BENITEZ OLVIN"
        },
        {
            "id": "002-08723",
            "nombre": "MALDONADO REYES JAIME ANIBAL"
        },
        {
            "id": "002-08726",
            "nombre": "BACA CHACON CARLOS JEOVANNY"
        },
        {
            "id": "002-08744",
            "nombre": "VASQUEZ CARLOS ARTURO"
        },
        {
            "id": "002-08747",
            "nombre": "ZELAYA AGURCIA RAMON"
        },
        {
            "id": "002-08768",
            "nombre": "SOTO CERVANTES ELA DINORA"
        },
        {
            "id": "002-08772",
            "nombre": "RODRIGUEZ MATAMOROS CARLOS SANTOS"
        },
        {
            "id": "005-00015",
            "nombre": "LAZARUS SERVICE, S.A. de C.V."
        },
        {
            "id": "006-00001",
            "nombre": "OLIVA ORDOÑEZ STANLEY"
        },
        {
            "id": "007-00011",
            "nombre": "CONSTRUCTORA MONCADA"
        },
        {
            "id": "007-00012",
            "nombre": "ALCALDIA MUNICIPAL DE COMAYAGUA"
        }
    ]
