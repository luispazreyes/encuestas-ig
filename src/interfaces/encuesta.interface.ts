export interface Encuesta {
  hallazgo?: string;
  algo_mas?: string;
  coord_x?: number;
  coord_y?: number;
  sexo?: string;
  pregunta1?: string;
}
