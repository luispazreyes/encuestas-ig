import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ConfiguracionPage, EncuestasPage, EncuestaDinamicaPage, EncuestasEnviadasPage, ModalMapPage, EncuestaDescargarPage } from '../index.page';

import { EncuestasProvider } from '../../providers/index.provider';

/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tab1Root: any = EncuestasPage;
  tab2Root: any = EncuestasEnviadasPage;
  tab3Root: any = EncuestaDescargarPage;
  tab4Root: any = ConfiguracionPage;

  constructor( public navCtrl: NavController, public navParams: NavParams,
  public _ep: EncuestasProvider ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
