import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EncuestaDescargarPage } from './encuesta-descargar';

@NgModule({
  declarations: [
    EncuestaDescargarPage,
  ],
  imports: [
    IonicPageModule.forChild(EncuestaDescargarPage),
  ],
})
export class EncuestaDescargarPageModule {}
