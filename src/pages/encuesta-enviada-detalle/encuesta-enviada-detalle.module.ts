import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EncuestaEnviadaDetallePage } from './encuesta-enviada-detalle';

@NgModule({
  declarations: [
    EncuestaEnviadaDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(EncuestaEnviadaDetallePage),
  ],
})
export class EncuestaEnviadaDetallePageModule {}
